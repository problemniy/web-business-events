package com.tools.wbe.manage.executor.abs;

import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;
import com.tools.wbe.data.event.WebBusinessActionEvent;
import com.tools.wbe.data.exception.WebBusinessException;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Supplier;

public abstract class AWebEventExecutor<EV extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> implements ApplicationEventPublisherAware {
    private final Queue<String> actionsQueue = new PriorityQueue<>();

    private ApplicationEventPublisher publisher;
    private boolean businessError = false;

    @Override
    public void setApplicationEventPublisher(@Nullable ApplicationEventPublisher applicationEventPublisher) {
        Assert.notNull(applicationEventPublisher, "applicationEventPublisher cannot be null");
        publisher = applicationEventPublisher;
    }

    public AWebEventExecutor<EV, REQ, RES> prepare(Supplier<REQ> requestContext, Supplier<RES> responseContext) {
        REQ reqCont = requestContext.get();
        RES resCont = responseContext.get();
        getAction().create(reqCont, resCont).attachTo(this::execute).attachBusinessExceptionTo(this::handleWebBusinessException);
        if (reqCont.getActionName() != null) {
            actionsQueue.add(reqCont.getActionName());
        }
        return this;
    }

    public AWebEventExecutor<EV, REQ, RES> addAction(String actionName) {
        actionsQueue.add(actionName);
        return this;
    }

    public AWebEventExecutor<EV, REQ, RES> addActions(Collection<String> actionNames) {
        actionsQueue.addAll(actionNames);
        return this;
    }

    public AWebEventExecutor<EV, REQ, RES> addProperties(Map<String, Object> properties) {
        getAction().addProperties(properties);
        return this;
    }

    public AWebEventExecutor<EV, REQ, RES> addProperty(String key, Object object) {
        getAction().addProperty(key, object);
        return this;
    }

    public Result go() {
        Assert.notEmpty(actionsQueue, "No one action was specified for web business event!");
        getAction().go();
        return new Result();
    }

    protected abstract EV getAction();

    @Transactional(propagation = Propagation.REQUIRED)
    public void execute(AWebRequestContext requestContext, AWebResponseContext responseContext) {
        while (!actionsQueue.isEmpty() && !businessError) {
            String actionName = actionsQueue.poll();
            REQ context = getAction().getRequestContext();
            if (!actionName.equals(context.getActionName())) {
                context.setActionName(actionName);
            }
            ApplicationEvent appEvent = new WebBusinessActionEvent(getAction());
            publisher.publishEvent(appEvent);
        }
    }

    private void handleWebBusinessException(WebBusinessException webBusinessException) {
        businessError = true;
    }

    protected Queue<String> getActionsQueue() {
        return actionsQueue;
    }

    public class Result {

        private Result() {

        }

        public RES asResponseContext() {
            return getAction().getResponseContext();
        }

        @Nullable
        public ResponseEntity<Serializable> asResponseEntity() {
            return getAction().getResponseEntity();
        }

        public HttpHeaders asHeaders() {
            return getAction().getRequestContext().getHeaders();
        }

        @Nullable
        public <Re extends Serializable> ResponseEntity<Re> asResponseEntity(Class<Re> bodyType) {
            ResponseEntity<Serializable> response = getAction().getResponseEntity();
            return response != null ? new ResponseEntity<>(bodyType.cast(response.getBody()), response.getHeaders(), response.getStatusCode()) : null;
        }

        public Result getProperties(Consumer<Map<String, Object>> handle) {
            handle.accept(getAction().getBusinessProperties().getProperties());
            return this;
        }

        public Result getProperty(String key, Consumer<Object> handle) {
            handle.accept(getAction().getProperty(key));
            return this;
        }

        public <T> Result getProperty(String key, Class<T> clazz, Consumer<T> handle) {
            handle.accept(getAction().getProperty(key, clazz));
            return this;
        }
    }
}
