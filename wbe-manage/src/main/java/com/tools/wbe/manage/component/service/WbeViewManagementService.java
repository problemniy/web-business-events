package com.tools.wbe.manage.component.service;

import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.data.model.view.WbeProcessorInvolvementViewModel;
import com.tools.wbe.data.model.view.WbeProcessorViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class WbeViewManagementService implements IWbeViewManagementService {

    private final Map<String, List<Pair<IPostActionProcessor, Integer>>> postActionProcessorsMapping;
    private final Map<String, List<Pair<IPutActionProcessor, Integer>>> putActionProcessorsMapping;
    private final Map<String, List<Pair<IGetActionProcessor, Integer>>> getActionProcessorsMapping;
    private final Map<String, List<Pair<IHeadActionProcessor, Integer>>> headActionProcessorsMapping;

    @Autowired
    public WbeViewManagementService(
            @Qualifier("wbePostActionProcessorsMapping") Map<String, List<Pair<IPostActionProcessor, Integer>>> postActionProcessorsMapping,
            @Qualifier("wbePutActionProcessorsMapping") Map<String, List<Pair<IPutActionProcessor, Integer>>> putActionProcessorsMapping,
            @Qualifier("wbeGetActionProcessorsMapping") Map<String, List<Pair<IGetActionProcessor, Integer>>> getActionProcessorsMapping,
            @Qualifier("wbeHeadActionProcessorsMapping") Map<String, List<Pair<IHeadActionProcessor, Integer>>> headActionProcessorsMapping) {
        this.postActionProcessorsMapping = postActionProcessorsMapping;
        this.putActionProcessorsMapping = putActionProcessorsMapping;
        this.getActionProcessorsMapping = getActionProcessorsMapping;
        this.headActionProcessorsMapping = headActionProcessorsMapping;
    }

    @Override
    public void generateMappingModels(Model model, String key) {
        Map<String, List<Pair<List<Pair<IWebEventProcessor, Integer>>, String>>> involvementsMapping = new HashMap<>();
        mergeProcessorInvolvementsWithMappingByActionsAndEventTypes(involvementsMapping, postActionProcessorsMapping);
        mergeProcessorInvolvementsWithMappingByActionsAndEventTypes(involvementsMapping, putActionProcessorsMapping);
        mergeProcessorInvolvementsWithMappingByActionsAndEventTypes(involvementsMapping, getActionProcessorsMapping);
        mergeProcessorInvolvementsWithMappingByActionsAndEventTypes(involvementsMapping, headActionProcessorsMapping);
        Collection<WbeProcessorInvolvementViewModel> involvementModels = generateViewInvolvementModels(involvementsMapping);
        model.addAttribute(key, involvementModels);
    }

    /* structure of collection to merge:
     * key is action name
     * value consists of Pair[List<Pair[processor, order]>, event type]
     */
    private <T extends IWebEventProcessor> void mergeProcessorInvolvementsWithMappingByActionsAndEventTypes(
            Map<String, List<Pair<List<Pair<IWebEventProcessor, Integer>>, String>>> mapToMerge, Map<String, List<Pair<T, Integer>>> actionProcessorsMapping) {
        Map<String, List<Pair<IWebEventProcessor, Integer>>> preConvertedMap = new HashMap<>();
        // convert children processors to IWebEventProcessor class
        actionProcessorsMapping.forEach((actionName, pairProcessor) -> preConvertedMap.put(actionName, pairProcessor.stream()
                .map((Function<Pair<T, Integer>, Pair<IWebEventProcessor, Integer>>) tIntegerPair -> Pair.of(tIntegerPair.getFirst(), tIntegerPair.getSecond()))
                .collect(Collectors.toList())));
        // merge processors
        preConvertedMap.forEach((actionName, processorsPairs) -> {
            List<Pair<List<Pair<IWebEventProcessor, Integer>>, String>> pairs = mapToMerge.get(actionName);
            for (Map.Entry<String, List<Pair<IWebEventProcessor, Integer>>> mappedProcessors : getMappedProcessorsToWbeRequestMethodNames(actionName, processorsPairs).entrySet()) {
                if (pairs == null) {
                    List<Pair<List<Pair<IWebEventProcessor, Integer>>, String>> newList = mapToMerge.computeIfAbsent(actionName, k -> new ArrayList<>());
                    newList.add(Pair.of(mappedProcessors.getValue(), mappedProcessors.getKey()));
                } else {
                    boolean checked = false;
                    for (Pair<List<Pair<IWebEventProcessor, Integer>>, String> p : pairs) {
                        if (p.getSecond().equals(mappedProcessors.getKey())) {
                            List<Pair<IWebEventProcessor, Integer>> toMerge = new ArrayList<>(mappedProcessors.getValue());
                            toMerge.removeAll(p.getFirst());
                            p.getFirst().addAll(toMerge);
                            p.getFirst().sort(Comparator.comparing(Pair::getSecond));
                            checked = true;
                            break;
                        }
                    }
                    if (!checked) {
                        pairs.add(Pair.of(mappedProcessors.getValue(), mappedProcessors.getKey()));
                    }
                }
            }
        });
    }

    private <T extends IWebEventProcessor> Map<String, List<Pair<IWebEventProcessor, Integer>>>
    getMappedProcessorsToWbeRequestMethodNames(String actionName, List<Pair<IWebEventProcessor, Integer>> processorPairs) {
        Map<String, List<Pair<IWebEventProcessor, Integer>>> result = new HashMap<>();
        processorPairs.forEach(processorPositionPair -> {
            calculateMappedProcessorsToWbeRequestMethodNames(result, actionName, EWbeRequestMethod.POST.name(), processorPositionPair, postActionProcessorsMapping, IPostActionProcessor.class);
            calculateMappedProcessorsToWbeRequestMethodNames(result, actionName, EWbeRequestMethod.PUT.name(), processorPositionPair, putActionProcessorsMapping, IPutActionProcessor.class);
            calculateMappedProcessorsToWbeRequestMethodNames(result, actionName, EWbeRequestMethod.GET.name(), processorPositionPair, getActionProcessorsMapping, IGetActionProcessor.class);
            calculateMappedProcessorsToWbeRequestMethodNames(result, actionName, EWbeRequestMethod.HEAD.name(), processorPositionPair, headActionProcessorsMapping, IHeadActionProcessor.class);
        });
        return result;
    }

    private <PRO extends IWebEventProcessor>
    void calculateMappedProcessorsToWbeRequestMethodNames(Map<String, List<Pair<IWebEventProcessor, Integer>>> mapToFill, String actionName, String requestMethodName,
                                                          Pair<IWebEventProcessor, Integer> processorPositionPair, Map<String, List<Pair<PRO, Integer>>> processorsMapping, Class<PRO> processorType) {
        if (processorType.isInstance(processorPositionPair.getFirst())) {
            List<Pair<PRO, Integer>> availableProcessors = processorsMapping.get(actionName);
            if (availableProcessors != null && availableProcessors.contains(processorPositionPair)) {
                List<Pair<IWebEventProcessor, Integer>> processors = mapToFill.computeIfAbsent(requestMethodName, k -> new ArrayList<>());
                processors.add(Pair.of(processorPositionPair.getFirst(), processorPositionPair.getSecond()));
            }
        }
    }

    private Collection<WbeProcessorInvolvementViewModel> generateViewInvolvementModels(
            Map<String, List<Pair<List<Pair<IWebEventProcessor, Integer>>, String>>> actionProcessorsMapping) {
        Collection<WbeProcessorInvolvementViewModel> viewInvolvements = new ArrayList<>();
        for (Map.Entry<String, List<Pair<List<Pair<IWebEventProcessor, Integer>>, String>>> entry : actionProcessorsMapping.entrySet()) {
            WbeProcessorInvolvementViewModel wbeInvolvement = new WbeProcessorInvolvementViewModel(entry.getKey(), Collections.emptyList());
            for (Pair<List<Pair<IWebEventProcessor, Integer>>, String> pairPro : entry.getValue()) {
                wbeInvolvement.addProcessors(pairPro.getFirst().stream()
                        .map(innerPair -> new WbeProcessorViewModel(innerPair.getSecond(), innerPair.getFirst().getClass().getName(), pairPro.getSecond())).collect(Collectors.toList()));
            }
            viewInvolvements.add(wbeInvolvement);
        }
        return viewInvolvements;
    }
}
