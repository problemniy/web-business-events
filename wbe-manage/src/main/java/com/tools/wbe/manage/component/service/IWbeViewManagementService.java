package com.tools.wbe.manage.component.service;

import org.springframework.ui.Model;

public interface IWbeViewManagementService {

    void generateMappingModels(Model model, String key);
}
