package com.tools.wbe.manage.executor;

import com.tools.wbe.data.business.event.PutAction;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;

public class PutActionExecutor extends AWebEventExecutor<PutAction, PutRequestContext, PutResponseContext> {

    private final PutAction putAction = new PutAction();

    @Override
    protected PutAction getAction() {
        return putAction;
    }
}
