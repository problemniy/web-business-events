package com.tools.wbe.manage.executor;

import com.tools.wbe.data.business.event.GetAction;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;

public class GetActionExecutor extends AWebEventExecutor<GetAction, GetRequestContext, GetResponseContext> {

    private final GetAction getAction = new GetAction();

    @Override
    protected GetAction getAction() {
        return getAction;
    }
}
