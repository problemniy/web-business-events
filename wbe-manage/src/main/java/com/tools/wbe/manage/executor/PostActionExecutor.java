package com.tools.wbe.manage.executor;

import com.tools.wbe.data.business.event.PostAction;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;

public class PostActionExecutor extends AWebEventExecutor<PostAction, PostRequestContext, PostResponseContext> {

    private final PostAction postAction = new PostAction();

    @Override
    protected PostAction getAction() {
        return postAction;
    }
}
