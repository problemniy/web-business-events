package com.tools.wbe.manage.executor;

import com.tools.wbe.data.business.event.HeadAction;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;

public class HeadActionExecutor extends AWebEventExecutor<HeadAction, HeadRequestContext, HeadResponseContext> {

    private final HeadAction headAction = new HeadAction();

    @Override
    protected HeadAction getAction() {
        return headAction;
    }
}
