package com.tools.wbe.data.func;

import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;

@FunctionalInterface
public interface WebEventExecuteHandler {

    void accept(AWebRequestContext requestContext, AWebResponseContext responseContext);
}
