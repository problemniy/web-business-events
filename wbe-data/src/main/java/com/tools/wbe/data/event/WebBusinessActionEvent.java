package com.tools.wbe.data.event;

import com.tools.wbe.data.business.event.abs.AWebEvent;
import org.springframework.context.ApplicationEvent;

public class WebBusinessActionEvent extends ApplicationEvent {

    public WebBusinessActionEvent(AWebEvent source) {
        super(source);
    }
}
