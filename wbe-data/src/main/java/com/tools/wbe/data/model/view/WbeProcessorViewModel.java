package com.tools.wbe.data.model.view;

import java.io.Serializable;

public class WbeProcessorViewModel implements Serializable {

    private int order;
    private String name;
    private String eventType;

    public WbeProcessorViewModel() {
    }

    public WbeProcessorViewModel(int order, String name, String eventType) {
        this.order = order;
        this.name = name;
        this.eventType = eventType;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
