package com.tools.wbe.data.context;

import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.type.IWebBodyContext;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

public class PostRequestContext extends AWebRequestContext implements IWebBodyContext {

    public PostRequestContext(HttpHeaders headers, HttpServletRequest request) {
        super(headers, request);
    }

    public PostRequestContext(HttpHeaders headers, HttpServletRequest request, String actionName) {
        super(headers, request, actionName);
    }

    public PostRequestContext(HttpHeaders headers, HttpServletRequest request, @Nullable Serializable body, String actionName) {
        super(headers, request, body, actionName);
    }

    @Override
    public Serializable getBody() {
        return body;
    }

    @Nullable
    @Override
    public <T extends Serializable> T getBody(Class<T> bodyType) {
        return bodyType.isInstance(body) ? bodyType.cast(body) : null;
    }

    @Override
    public void setBody(Serializable body) {
        this.body = body;
    }
}
