package com.tools.wbe.data.context.abs;

import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

public abstract class AWebRequestContext extends AWebContext {
    private final HttpServletRequest request;
    protected String actionName;

    public AWebRequestContext(HttpHeaders headers, HttpServletRequest request) {
        super(headers);
        this.request = request;
    }

    public AWebRequestContext(HttpHeaders headers, HttpServletRequest request, String actionName) {
        super(headers);
        this.request = request;
        this.actionName = actionName;
    }

    public AWebRequestContext(HttpHeaders headers, HttpServletRequest request, @Nullable Serializable body, String actionName) {
        super(headers, body);
        this.request = request;
        this.actionName = actionName;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }
}
