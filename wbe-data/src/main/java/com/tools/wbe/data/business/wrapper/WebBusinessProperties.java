package com.tools.wbe.data.business.wrapper;

import org.springframework.lang.Nullable;

import java.util.Map;

public class WebBusinessProperties {

    private final Map<String, Object> properties;

    public WebBusinessProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public void addProperties(Map<String, Object> properties) {
        this.properties.putAll(properties);
    }

    public void addProperty(String propertyKey, Object propertyObject) {
        properties.put(propertyKey, propertyObject);
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    @Nullable
    public <T> T getProperty(String key, Class<T> clazz) {
        Object obj = properties.get(key);
        return clazz.isInstance(obj) ? clazz.cast(obj) : null;
    }

    @Nullable
    public Object getProperty(String key) {
        return properties.get(key);
    }
}
