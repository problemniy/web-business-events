package com.tools.wbe.data.business;

import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;

import java.util.List;

public class WebEventProcessorInvolvement<PRO extends IWebEventProcessor> {

    protected List<EWbeRequestMethod> eventTypes;
    protected List<String> actions;
    protected PRO processor;
    protected int order;

    public List<EWbeRequestMethod> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EWbeRequestMethod> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }

    public PRO getProcessor() {
        return processor;
    }

    public void setProcessor(PRO processor) {
        this.processor = processor;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
