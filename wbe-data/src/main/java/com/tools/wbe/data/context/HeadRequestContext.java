package com.tools.wbe.data.context;

import com.tools.wbe.data.context.abs.AWebRequestContext;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

public class HeadRequestContext extends AWebRequestContext {

    public HeadRequestContext(HttpHeaders headers, HttpServletRequest request) {
        super(headers, request);
    }

    public HeadRequestContext(HttpHeaders headers, HttpServletRequest request, String actionName) {
        super(headers, request, actionName);
    }
}
