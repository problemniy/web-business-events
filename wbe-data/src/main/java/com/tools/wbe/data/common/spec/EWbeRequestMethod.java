package com.tools.wbe.data.common.spec;

public enum EWbeRequestMethod {
    GET, POST, HEAD, PUT
}
