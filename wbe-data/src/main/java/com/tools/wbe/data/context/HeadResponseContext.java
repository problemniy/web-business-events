package com.tools.wbe.data.context;

import com.tools.wbe.data.context.abs.AWebResponseContext;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletResponse;

public class HeadResponseContext extends AWebResponseContext {

    public HeadResponseContext(HttpHeaders headers, HttpServletResponse response) {
        super(headers, response);
    }
}
