package com.tools.wbe.data.business.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;

public interface IPutActionProcessor extends IWebEventProcessor {

    void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess);
}
