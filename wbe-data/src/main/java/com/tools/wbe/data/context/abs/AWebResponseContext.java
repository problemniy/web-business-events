package com.tools.wbe.data.context.abs;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public abstract class AWebResponseContext extends AWebContext {
    private final HttpServletResponse response;
    protected HttpStatus httpStatus = HttpStatus.OK;


    public AWebResponseContext(HttpHeaders headers, HttpServletResponse response) {
        super(headers);
        this.response = response;
    }

    public AWebResponseContext(HttpHeaders headers, HttpServletResponse response, @Nullable Serializable body) {
        super(headers, body);
        this.response = response;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
