package com.tools.wbe.data.context;

import com.tools.wbe.data.context.abs.AWebResponseContext;
import com.tools.wbe.data.context.type.IWebBodyContext;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public class PostResponseContext extends AWebResponseContext implements IWebBodyContext {

    public PostResponseContext(HttpHeaders headers, HttpServletResponse response) {
        super(headers, response);
    }

    public PostResponseContext(HttpHeaders headers, HttpServletResponse response, @Nullable Serializable body) {
        super(headers, response, body);
    }

    @Override
    public Serializable getBody() {
        return body;
    }

    @Nullable
    @Override
    public <T extends Serializable> T getBody(Class<T> bodyType) {
        return bodyType.isInstance(body) ? bodyType.cast(body) : null;
    }

    @Override
    public void setBody(Serializable body) {
        this.body = body;
    }
}
