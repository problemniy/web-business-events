package com.tools.wbe.data.model.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WbeProcessorInvolvementViewModel implements Serializable {

    private String actionName;
    private final List<WbeProcessorViewModel> processors = new ArrayList<>();

    public WbeProcessorInvolvementViewModel() {
    }

    public WbeProcessorInvolvementViewModel(String actionName, List<WbeProcessorViewModel> processors) {
        this.actionName = actionName;
        this.processors.addAll(processors);
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public List<WbeProcessorViewModel> getProcessors() {
        return processors;
    }

    public void addProcessor(WbeProcessorViewModel processor) {
        processors.add(processor);
    }

    public void addProcessors(Collection<WbeProcessorViewModel> processors) {
        this.processors.addAll(processors);
    }
}
