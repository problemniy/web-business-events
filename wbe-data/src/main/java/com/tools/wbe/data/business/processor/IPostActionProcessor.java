package com.tools.wbe.data.business.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;

public interface IPostActionProcessor extends IWebEventProcessor {

    void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess);
}
