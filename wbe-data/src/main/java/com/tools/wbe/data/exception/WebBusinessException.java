package com.tools.wbe.data.exception;

import org.springframework.http.HttpStatus;

public class WebBusinessException extends Exception {

    private final HttpStatus httpStatus;

    public WebBusinessException() {
        super();
        this.httpStatus = HttpStatus.OK;
    }

    public WebBusinessException(String errorMessage) {
        super(errorMessage);
        this.httpStatus = HttpStatus.OK;
    }

    public WebBusinessException(HttpStatus httpStatus) {
        super();
        this.httpStatus = httpStatus;
    }

    public WebBusinessException(HttpStatus httpStatus, String errorMessage) {
        super(errorMessage);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
