package com.tools.wbe.data.builder;

import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.processor.IWebEventProcessor;

import java.util.ArrayList;
import java.util.List;

public class WebBusinessEventsBuilder {

    private List<WebEventProcessorInvolvement<IWebEventProcessor>> webEventProcessorsInvolvements = new ArrayList<>();

    public List<WebEventProcessorInvolvement<IWebEventProcessor>> getWebEventProcessorsInvolvements() {
        return webEventProcessorsInvolvements;
    }

    public void setWebEventProcessorsInvolvements(List<WebEventProcessorInvolvement<IWebEventProcessor>> webEventProcessorsInvolvements) {
        this.webEventProcessorsInvolvements = webEventProcessorsInvolvements;
    }
}
