package com.tools.wbe.data.business.event;

import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;

public class GetAction extends AWebEvent<GetRequestContext, GetResponseContext> {

    @Override
    protected ResponseEntity<Serializable> prepareResponseEntity() {
        return new ResponseEntity<>(getResponseContext().getBody(), getResponseContext().getHeaders(), getResponseContext().getHttpStatus());
    }
}
