package com.tools.wbe.data.business.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;

public interface IGetActionProcessor extends IWebEventProcessor {

    void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess);
}
