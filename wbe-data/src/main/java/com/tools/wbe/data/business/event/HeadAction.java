package com.tools.wbe.data.business.event;

import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

import java.io.Serializable;

public class HeadAction extends AWebEvent<HeadRequestContext, HeadResponseContext> {

    @Override
    protected ResponseEntity<Serializable> prepareResponseEntity() {
        // this action should not have response
        return null;
    }

    @Override
    @Nullable
    public ResponseEntity<Serializable> getResponseEntity() {
        return super.getResponseEntity();
    }
}
