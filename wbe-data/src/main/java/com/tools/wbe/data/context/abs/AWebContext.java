package com.tools.wbe.data.context.abs;

import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;

import java.io.Serializable;

public abstract class AWebContext {

    private final HttpHeaders headers;
    // should be changed by children only
    protected Serializable body;

    public AWebContext(HttpHeaders headers) {
        this.headers = headers;
    }

    public AWebContext(HttpHeaders headers, @Nullable Serializable body) {
        this.headers = headers;
        this.body = body;
    }

    public HttpHeaders getHeaders() {
        return headers;
    }
}
