package com.tools.wbe.data.business.delegate;

import com.tools.wbe.data.business.wrapper.WebBusinessProperties;
import com.tools.wbe.data.common.spec.EWebEventStrategy;

public interface IWebBusinessProcess {

    void sendStrategy(EWebEventStrategy strategy);

    WebBusinessProperties properties();
}
