package com.tools.wbe.data.business.event;

import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;

public class PostAction extends AWebEvent<PostRequestContext, PostResponseContext> {

    @Override
    protected ResponseEntity<Serializable> prepareResponseEntity() {
        return new ResponseEntity<>(getResponseContext().getBody(), getResponseContext().getHeaders(), getResponseContext().getHttpStatus());
    }
}
