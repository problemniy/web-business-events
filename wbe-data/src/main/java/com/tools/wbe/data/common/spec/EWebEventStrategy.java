package com.tools.wbe.data.common.spec;

public enum EWebEventStrategy {
    CONTINUE, BREAK, ERROR
}
