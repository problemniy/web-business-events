package com.tools.wbe.data.business.event.abs;

import com.tools.wbe.data.business.wrapper.WebBusinessProperties;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;
import com.tools.wbe.data.exception.WebBusinessException;
import com.tools.wbe.data.func.WebEventExceptionHandler;
import com.tools.wbe.data.func.WebEventExecuteHandler;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public abstract class AWebEvent<REQ extends AWebRequestContext, RES extends AWebResponseContext> {

    private final WebBusinessProperties webBusinessEventsProperties = new WebBusinessProperties(new HashMap<>());

    private REQ requestContext;
    private RES responseContext;

    private WebEventExecuteHandler onStartDelegate;
    private WebEventExceptionHandler onWebEventExceptionDelegate;

    private ResponseEntity<Serializable> responseEntity;

    protected AWebEvent() {

    }

    public void go() {
        Assert.notNull(requestContext, "requestContext cannot be null");
        Assert.notNull(responseContext, "responseContext cannot be null");
        if (onStartDelegate != null) {
            onStartDelegate.accept(requestContext, responseContext);
        }
        responseEntity = prepareResponseEntity();
    }

    public void eventException(WebBusinessException webBusinessException) {
        if (onWebEventExceptionDelegate != null) {
            onWebEventExceptionDelegate.handle(webBusinessException);
        }
    }

    protected abstract ResponseEntity<Serializable> prepareResponseEntity();

    public AWebEvent<REQ, RES> create(REQ requestContext, RES responseContext) {
        this.requestContext = requestContext;
        this.responseContext = responseContext;
        return this;
    }


    public AWebEvent<REQ, RES> attachTo(WebEventExecuteHandler onStartDelegate) {
        this.onStartDelegate = onStartDelegate;
        return this;
    }

    public AWebEvent<REQ, RES> attachBusinessExceptionTo(WebEventExceptionHandler onWebEventExceptionDelegate) {
        this.onWebEventExceptionDelegate = onWebEventExceptionDelegate;
        return this;
    }

    public AWebEvent<REQ, RES> addProperties(Map<String, Object> properties) {
        webBusinessEventsProperties.addProperties(properties);
        return this;
    }

    public AWebEvent<REQ, RES> addProperty(String propertyKey, Object propertyObject) {
        webBusinessEventsProperties.addProperty(propertyKey, propertyObject);
        return this;
    }

    @Nullable
    public <T> T getProperty(String key, Class<T> clazz) {
        return webBusinessEventsProperties.getProperty(key, clazz);
    }

    @Nullable
    public Object getProperty(String key) {
        return webBusinessEventsProperties.getProperty(key);
    }

    public WebBusinessProperties getBusinessProperties() {
        return webBusinessEventsProperties;
    }

    public REQ getRequestContext() {
        return requestContext;
    }

    public RES getResponseContext() {
        return responseContext;
    }

    public ResponseEntity<Serializable> getResponseEntity() {
        return responseEntity;
    }
}
