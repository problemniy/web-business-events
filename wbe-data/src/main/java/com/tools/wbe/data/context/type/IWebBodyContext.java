package com.tools.wbe.data.context.type;

import java.io.Serializable;

public interface IWebBodyContext {

    Serializable getBody();

    <T extends Serializable> T getBody(Class<T> bodyType);

    void setBody(Serializable body);
}
