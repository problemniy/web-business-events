package com.tools.wbe.data.business.event;

import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;

public class PutAction extends AWebEvent<PutRequestContext, PutResponseContext> {

    @Override
    protected ResponseEntity<Serializable> prepareResponseEntity() {
        return new ResponseEntity<>(getResponseContext().getBody(), getResponseContext().getHeaders(), getResponseContext().getHttpStatus());
    }
}
