package com.tools.wbe.data.func;

import com.tools.wbe.data.exception.WebBusinessException;

@FunctionalInterface
public interface WebEventExceptionHandler {

    void handle(WebBusinessException webBusinessException);
}
