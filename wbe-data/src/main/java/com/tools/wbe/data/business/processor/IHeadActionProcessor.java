package com.tools.wbe.data.business.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;

public interface IHeadActionProcessor extends IWebEventProcessor {

    void execute(HeadRequestContext requestContext, HeadResponseContext responseContext, IWebBusinessProcess businessProcess);
}
