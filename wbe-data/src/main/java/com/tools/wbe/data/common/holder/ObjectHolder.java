package com.tools.wbe.data.common.holder;

public class ObjectHolder<T> {
    private T object;

    public ObjectHolder(T object) {
        this.object = object;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }
}
