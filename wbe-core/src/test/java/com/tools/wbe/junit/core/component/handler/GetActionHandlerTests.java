package com.tools.wbe.junit.core.component.handler;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.junit.core.processor.GetActionTestProcessor;
import com.tools.wbe.manage.executor.GetActionExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WbeManagementConfiguration.class, GetActionHandlerTests.OwnWebBusinessEventsConfiguration.class})
public class GetActionHandlerTests {

    private static final String TEST_ACTION_1 = "TEST_1";
    private static final String TEST_ACTION_2 = "TEST_2";

    @Autowired
    private Map<String, List<Pair<IGetActionProcessor, Integer>>> getActionProcessorsMapping;
    @Autowired
    private GetActionExecutor getActionExecutor;
    @Autowired
    private WebBusinessEventsService webBusinessEventsService;

    private static final Map<IWebEventProcessor, WebEventProcessorInvolvement<IWebEventProcessor>> PROCESSORS = new HashMap<>() {{
        WebBusinessEventsService webBusinessEventsService = new WebBusinessEventsService();
        IWebEventProcessor processor_1 = Mockito.mock(GetActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> getInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.GET), Arrays.asList(TEST_ACTION_1, TEST_ACTION_2), 3, processor_1);
        IWebEventProcessor processor_2 = Mockito.mock(GetActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> getInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.GET), Collections.singletonList(TEST_ACTION_2), 11, processor_2);
        put(processor_1, getInvolvement_1);
        put(processor_2, getInvolvement_2);
    }};

    @Configuration
    static class OwnWebBusinessEventsConfiguration extends AWebBusinessEventsConfigAdapter {

        @Override
        protected void configure(WebBusinessEventsBuilder wbe) {
            wbe.setWebEventProcessorsInvolvements(new ArrayList<>(PROCESSORS.values()));
        }
    }

    @Test
    public void checkGetActionHandlerInvokesProcessors() {
        getActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, GetRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(null, GetResponseContext.class))
                .addAction(TEST_ACTION_1).go();
        for (Map.Entry<String, List<Pair<IGetActionProcessor, Integer>>> entry : getActionProcessorsMapping.entrySet()) {
            for (Pair<IGetActionProcessor, Integer> processorPair : entry.getValue()) {
                Mockito.verify(processorPair.getFirst(), Mockito.times(PROCESSORS.get(processorPair.getFirst()).getActions().contains(TEST_ACTION_1) ? 1 : 0))
                        .execute(any(GetRequestContext.class), any(GetResponseContext.class), any(IWebBusinessProcess.class));
            }
        }
    }
}
