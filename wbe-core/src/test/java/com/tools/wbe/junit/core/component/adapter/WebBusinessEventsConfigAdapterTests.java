package com.tools.wbe.junit.core.component.adapter;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.core.config.WbeEmptyConfiguration;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.IsNot.not;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WbeManagementConfiguration.class, WebBusinessEventsConfigAdapterTests.OwnWebBusinessEventsConfiguration.class})
public class WebBusinessEventsConfigAdapterTests {

    @Autowired
    private AWebBusinessEventsConfigAdapter webBusinessEventsConfigAdapter;

    @Configuration
    static class OwnWebBusinessEventsConfiguration extends AWebBusinessEventsConfigAdapter {

        @Override
        protected void configure(WebBusinessEventsBuilder wbe) {
        }
    }

    @Test
    public void testCustomWbeConfigurationAvailableTest() {
        Assert.assertThat("If the abstract class " + AWebBusinessEventsConfigAdapter.class.getName() +
                        " has specified custom bean then it's type should not be " + WbeEmptyConfiguration.class.getName(),
                webBusinessEventsConfigAdapter, not(instanceOf(WbeEmptyConfiguration.class)));
    }
}
