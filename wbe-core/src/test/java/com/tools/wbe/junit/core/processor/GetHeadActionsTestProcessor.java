package com.tools.wbe.junit.core.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;

public class GetHeadActionsTestProcessor implements IGetActionProcessor, IHeadActionProcessor {

    @Override
    public void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess) {
    }

    @Override
    public void execute(HeadRequestContext requestContext, HeadResponseContext responseContext, IWebBusinessProcess businessProcess) {
    }
}
