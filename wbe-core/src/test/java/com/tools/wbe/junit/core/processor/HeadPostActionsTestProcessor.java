package com.tools.wbe.junit.core.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;

public class HeadPostActionsTestProcessor implements IHeadActionProcessor, IPostActionProcessor {

    @Override
    public void execute(HeadRequestContext requestContext, HeadResponseContext responseContext, IWebBusinessProcess businessProcess) {
    }

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
    }
}
