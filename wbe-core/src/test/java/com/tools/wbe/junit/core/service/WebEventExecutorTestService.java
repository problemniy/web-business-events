package com.tools.wbe.junit.core.service;

import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;
import com.tools.wbe.data.event.WebBusinessActionEvent;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("unchecked")
public class WebEventExecutorTestService implements IWebEventExecutorTestService {

    private static final String TEST_ACTION_1 = "testAction_1";
    private static final String TEST_ACTION_2 = "testAction_2";

    private final WebBusinessEventsService webBusinessEventsService;

    public WebEventExecutorTestService(WebBusinessEventsService webBusinessEventsService) {
        this.webBusinessEventsService = webBusinessEventsService;
    }

    @Override
    public <A extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> void checkContextActionIsCalled(
            ApplicationListener<WebBusinessActionEvent> actionHandler, AWebEventExecutor<A, REQ, RES> executor,
            Class<REQ> requestContextType, Class<RES> responseContextType) {
        Mockito.clearInvocations(actionHandler);
        executor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, null, TEST_ACTION_1, requestContextType),
                () -> webBusinessEventsService.prepareResponseContext(null, responseContextType))
                .go();
        Mockito.verify(actionHandler, Mockito.times(1)).onApplicationEvent(any(WebBusinessActionEvent.class));
    }

    @Override
    public <A extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> void checkExecutorActionsAreCalled(
            ApplicationListener<WebBusinessActionEvent> actionHandler, AWebEventExecutor<A, REQ, RES> executor,
            Class<REQ> requestContextType, Class<RES> responseContextType) {
        Mockito.clearInvocations(actionHandler);
        executor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, requestContextType),
                () -> webBusinessEventsService.prepareResponseContext(null, responseContextType))
                .addAction(TEST_ACTION_1).addActions(Arrays.asList(TEST_ACTION_1, TEST_ACTION_2)).go();
        Mockito.verify(actionHandler, Mockito.times(3)).onApplicationEvent(any(WebBusinessActionEvent.class));
    }

    @Override
    public <A extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> void checkContextAndExecutorActionsAreCalled(
            ApplicationListener<WebBusinessActionEvent> actionHandler, AWebEventExecutor<A, REQ, RES> executor,
            Class<REQ> requestContextType, Class<RES> responseContextType) {
        Mockito.clearInvocations(actionHandler);
        executor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, null, TEST_ACTION_1, requestContextType),
                () -> webBusinessEventsService.prepareResponseContext(null, responseContextType))
                .addAction(TEST_ACTION_1).addActions(Arrays.asList(TEST_ACTION_1, TEST_ACTION_2)).go();
        Mockito.verify(actionHandler, Mockito.times(4)).onApplicationEvent(any(WebBusinessActionEvent.class));
    }

    @Override
    public <A extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> void checkExecutorFillEventProperties(
            ApplicationListener<WebBusinessActionEvent> actionHandler, AWebEventExecutor<A, REQ, RES> executor,
            Class<A> webEventType, Class<REQ> requestContextType, Class<RES> responseContextType) {
        Mockito.clearInvocations(actionHandler);
        String property_1 = "obj_1";
        String property_2 = "obj_2";
        executor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, null, TEST_ACTION_1, requestContextType),
                () -> webBusinessEventsService.prepareResponseContext(null, responseContextType))
                .addProperty(property_1, property_2).addProperties(new HashMap<>() {{
            put(property_2, property_1);
        }}).go();
        ArgumentCaptor<WebBusinessActionEvent> eventCaptor = ArgumentCaptor.forClass(WebBusinessActionEvent.class);
        Mockito.verify(actionHandler, Mockito.times(1)).onApplicationEvent(eventCaptor.capture());
        AWebEvent webEvent = (AWebEvent) eventCaptor.getValue().getSource();
        if (!webEventType.isInstance(webEvent)) {
            return;
        }
        A getAction = webEventType.cast(webEvent);
        assertEquals("Executor " + executor.getClass().getName() + " invokes web event with wrong action,", TEST_ACTION_1, getAction.getRequestContext().getActionName());
        assertEquals("Expected property \'" + property_1 + "=" + property_2 + "\' is missing in collection of properties: " +
                        getAction.getBusinessProperties().getProperties().toString() + " in executor " + executor.getClass().getName() + ",",
                property_2, getAction.getBusinessProperties().getProperties().get(property_1));
        assertEquals("Expected property \'" + property_2 + "=" + property_1 + "\' is missing in collection of properties: " +
                        getAction.getBusinessProperties().getProperties().toString() + " in executor " + executor.getClass().getName() + ",",
                property_1, getAction.getBusinessProperties().getProperties().get(property_2));
    }

    @Override
    public void checkExecutorHasEmptyFieldsOnNewRequest(String request, WebApplicationContext context) throws Exception {
        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        mockMvc.perform(get(request)).andExpect(status().isOk());
        mockMvc.perform(get(request)).andExpect(status().isOk());
    }
}
