package com.tools.wbe.junit.core.component.handler;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.junit.core.processor.PostActionTestProcessor;
import com.tools.wbe.manage.executor.PostActionExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WbeManagementConfiguration.class, PostActionHandlerTests.OwnWebBusinessEventsConfiguration.class})
public class PostActionHandlerTests {

    private static final String TEST_ACTION_1 = "TEST_1";
    private static final String TEST_ACTION_2 = "TEST_2";

    @Autowired
    private Map<String, List<Pair<IPostActionProcessor, Integer>>> postActionProcessorsMapping;
    @Autowired
    private PostActionExecutor postActionExecutor;
    @Autowired
    private WebBusinessEventsService webBusinessEventsService;

    private static final Map<IWebEventProcessor, WebEventProcessorInvolvement<IWebEventProcessor>> PROCESSORS = new HashMap<>() {{
        WebBusinessEventsService webBusinessEventsService = new WebBusinessEventsService();
        IWebEventProcessor processor_1 = Mockito.mock(PostActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> postInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.POST), Arrays.asList(TEST_ACTION_1, TEST_ACTION_2), 14, processor_1);
        IWebEventProcessor processor_2 = Mockito.mock(PostActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> postInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.POST), Collections.singletonList(TEST_ACTION_2), 14, processor_2);
        put(processor_1, postInvolvement_1);
        put(processor_2, postInvolvement_2);
    }};

    @Configuration
    static class OwnWebBusinessEventsConfiguration extends AWebBusinessEventsConfigAdapter {

        @Override
        protected void configure(WebBusinessEventsBuilder wbe) {
            wbe.setWebEventProcessorsInvolvements(new ArrayList<>(PROCESSORS.values()));
        }
    }

    @Test
    public void checkPostActionHandlerInvokesProcessors() {
        postActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, PostRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(null, PostResponseContext.class))
                .addAction(TEST_ACTION_1).go();
        for (Map.Entry<String, List<Pair<IPostActionProcessor, Integer>>> entry : postActionProcessorsMapping.entrySet()) {
            for (Pair<IPostActionProcessor, Integer> processorPair : entry.getValue()) {
                Mockito.verify(processorPair.getFirst(), Mockito.times(PROCESSORS.get(processorPair.getFirst()).getActions().contains(TEST_ACTION_1) ? 1 : 0))
                        .execute(any(PostRequestContext.class), any(PostResponseContext.class), any(IWebBusinessProcess.class));
            }
        }
    }
}
