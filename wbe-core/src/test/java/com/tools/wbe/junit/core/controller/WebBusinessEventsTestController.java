package com.tools.wbe.junit.core.controller;

import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;
import com.tools.wbe.manage.executor.GetActionExecutor;
import com.tools.wbe.manage.executor.HeadActionExecutor;
import com.tools.wbe.manage.executor.PostActionExecutor;
import com.tools.wbe.manage.executor.PutActionExecutor;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Queue;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.collection.IsMapWithSize.anEmptyMap;
import static org.hamcrest.core.Is.is;

@Controller
@RequestMapping("/wbe/tests")
public class WebBusinessEventsTestController {

    private static final String TEST_ACTION_1 = "testAction_1";
    private static final String TEST_ACTION_2 = "testAction_2";
    private static final String TEST_PROPERTY_1 = "testProperty_1";
    private static final String TEST_PROPERTY_2 = "testProperty_2";

    @Autowired
    private WebBusinessEventsService webBusinessEventsService;
    @Autowired
    private GetActionExecutor getActionExecutor;
    @Autowired
    private HeadActionExecutor headActionExecutor;
    @Autowired
    private PostActionExecutor postActionExecutor;
    @Autowired
    private PutActionExecutor putActionExecutor;

    @RequestMapping(value = "/testGetActionExecutorScope", method = RequestMethod.GET)
    public ResponseEntity<String> testGetActionExecutorScope(@RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return testExecutorScope(getActionExecutor, headers, request, response, GetRequestContext.class, GetResponseContext.class);
    }

    @RequestMapping(value = "/testHeadActionExecutorScope", method = RequestMethod.GET)
    public ResponseEntity<String> testHeadActionExecutorScope(@RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return testExecutorScope(headActionExecutor, headers, request, response, HeadRequestContext.class, HeadResponseContext.class);
    }

    @RequestMapping(value = "/testPostActionExecutorScope", method = RequestMethod.GET)
    public ResponseEntity<String> testPostActionExecutorScope(@RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return testExecutorScope(postActionExecutor, headers, request, response, PostRequestContext.class, PostResponseContext.class);
    }

    @RequestMapping(value = "/testPutActionExecutorScope", method = RequestMethod.GET)
    public ResponseEntity<String> testPutActionExecutorScope(@RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return testExecutorScope(putActionExecutor, headers, request, response, PutRequestContext.class, PutResponseContext.class);
    }

    private <EV extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext>
    ResponseEntity<String> testExecutorScope(AWebEventExecutor<EV, REQ, RES> executor, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response,
                                             Class<REQ> requestContextType, Class<RES> responseContextType)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        checkWebEventExecutorFieldsEmpty(executor);
        return executor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, requestContextType),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, responseContextType))
                .addAction(TEST_ACTION_1)
                .addActions(Collections.singletonList(TEST_ACTION_2))
                .addProperty(TEST_PROPERTY_1, TEST_PROPERTY_2)
                .addProperties(new HashMap<>() {{
                    put(TEST_PROPERTY_2, TEST_PROPERTY_1);
                }})
                .go().asResponseEntity(String.class);
    }

    @SuppressWarnings("unchecked")
    private <EV extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext>
    void checkWebEventExecutorFieldsEmpty(AWebEventExecutor<EV, REQ, RES> executor) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // check actions for empty
        Method getActionsQueueMethod = AWebEventExecutor.class.getDeclaredMethod("getActionsQueue");
        getActionsQueueMethod.setAccessible(true);
        Queue<String> actionsQueue = (Queue<String>) getActionsQueueMethod.invoke(executor);
        assertThat(executor.getClass().getName() + " should have empty actions before execution", actionsQueue, is(empty()));
        // check properties for empty
        Method getActionMethod = AWebEventExecutor.class.getDeclaredMethod("getAction");
        getActionMethod.setAccessible(true);
        AWebEvent<REQ, RES> webEvent = (AWebEvent<REQ, RES>) getActionMethod.invoke(executor);
        assertThat(executor.getClass().getName() + " should have empty properties before execution", webEvent.getBusinessProperties().getProperties(), is(anEmptyMap()));
    }
}
