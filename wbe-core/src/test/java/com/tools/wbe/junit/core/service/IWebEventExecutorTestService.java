package com.tools.wbe.junit.core.service;

import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;
import com.tools.wbe.data.event.WebBusinessActionEvent;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;
import org.springframework.context.ApplicationListener;
import org.springframework.web.context.WebApplicationContext;

public interface IWebEventExecutorTestService {

    <A extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> void checkContextActionIsCalled(
            ApplicationListener<WebBusinessActionEvent> actionHandler, AWebEventExecutor<A, REQ, RES> executor,
            Class<REQ> requestContextType, Class<RES> responseContextType);

    <A extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> void checkExecutorActionsAreCalled(
            ApplicationListener<WebBusinessActionEvent> actionHandler, AWebEventExecutor<A, REQ, RES> executor,
            Class<REQ> requestContextType, Class<RES> responseContextType);

    <A extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> void checkContextAndExecutorActionsAreCalled(
            ApplicationListener<WebBusinessActionEvent> actionHandler, AWebEventExecutor<A, REQ, RES> executor,
            Class<REQ> requestContextType, Class<RES> responseContextType);

    <A extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext> void checkExecutorFillEventProperties(
            ApplicationListener<WebBusinessActionEvent> actionHandler, AWebEventExecutor<A, REQ, RES> executor,
            Class<A> webEventType, Class<REQ> requestContextType, Class<RES> responseContextType);

    void checkExecutorHasEmptyFieldsOnNewRequest(String request, WebApplicationContext context) throws Exception;
}
