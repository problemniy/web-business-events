package com.tools.wbe.junit.core.component.executor;

import com.tools.wbe.core.component.handler.PostActionHandler;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.business.event.PostAction;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.junit.core.service.IWebEventExecutorTestService;
import com.tools.wbe.junit.core.service.WebEventExecutorTestService;
import com.tools.wbe.manage.executor.PostActionExecutor;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WbeManagementConfiguration.class, PostActionExecutorTests.OwnWebBusinessEventsConfiguration.class})
public class PostActionExecutorTests {

    private static final String TEST_POST_ACTION_EXECUTOR_SCOPE_REQUEST = "/wbe/tests/testPostActionExecutorScope";

    @Autowired
    private PostActionExecutor postActionExecutor;
    @Autowired
    private PostActionHandler postActionHandler;
    @Autowired
    private WebBusinessEventsService webBusinessEventsService;
    @Autowired
    private IWebEventExecutorTestService webEventExecutorTestService;
    @Autowired
    private WebApplicationContext context;

    @Configuration
    @ComponentScan("com.tools.wbe.junit.core.controller")
    static class OwnWebBusinessEventsConfiguration {

        @Autowired
        private WebBusinessEventsService webBusinessEventsService;

        @Bean
        public WebEventExecutorTestService webEventExecutorTestService() {
            return new WebEventExecutorTestService(webBusinessEventsService);
        }

        @Bean
        @Primary
        public PostActionHandler postActionHandler() {
            return Mockito.mock(PostActionHandler.class);
        }
    }

    @Test
    public void checkContextActionIsCalled() {
        webEventExecutorTestService.checkContextActionIsCalled(postActionHandler, postActionExecutor, PostRequestContext.class, PostResponseContext.class);
    }

    @Test
    public void checkExecutorActionsAreCalled() {
        webEventExecutorTestService.checkExecutorActionsAreCalled(postActionHandler, postActionExecutor, PostRequestContext.class, PostResponseContext.class);
    }

    @Test
    public void checkContextAndExecutorActionsAreCalled() {
        webEventExecutorTestService.checkContextAndExecutorActionsAreCalled(postActionHandler, postActionExecutor, PostRequestContext.class, PostResponseContext.class);
    }

    @Test
    public void checkExecutorHasEmptyFieldsOnNewRequest() throws Exception {
        webEventExecutorTestService.checkExecutorHasEmptyFieldsOnNewRequest(TEST_POST_ACTION_EXECUTOR_SCOPE_REQUEST, context);
    }

    @Test
    public void checkExecutorFillEventProperties() {
        webEventExecutorTestService.checkExecutorFillEventProperties(postActionHandler, postActionExecutor, PostAction.class, PostRequestContext.class, PostResponseContext.class);
    }

    @Test
    public void checkExecutorsGoHasValidResponseObjects() {
        String body = "body";
        AWebEventExecutor<PostAction, PostRequestContext, PostResponseContext>.Result result = postActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, body, "action", PostRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(null, null, body, PostResponseContext.class))
                .go();
        assertNotNull("Expected not null for " + HttpHeaders.class.getName() + " after completion of "
                + PostActionExecutor.class.getName(), result.asHeaders());
        assertNotNull("Expected not null for " + GetResponseContext.class.getName() + " after completion of "
                + PostActionExecutor.class.getName(), result.asResponseContext());
        assertNotNull("Expected not null for " + ResponseEntity.class.getName() + "<" + Serializable.class.getName() + "> after completion of "
                + PostActionExecutor.class.getName(), result.asResponseEntity());
        assertNotNull("Expected not null for " + ResponseEntity.class.getName() + "<" + String.class.getName() + "> after completion of "
                + PostActionExecutor.class.getName(), result.asResponseEntity(String.class));
        String requestBody = Objects.requireNonNull(result.asResponseEntity(String.class)).getBody();
        assertEquals("Request entity should have body of type " + body.getClass().getName() +
                ", but actual type is " + (requestBody != null ? requestBody.getClass().getName() : "null") + ", executor: " +
                PostActionExecutor.class.getName() + ",", body, requestBody);
        String responseBody = Objects.requireNonNull(result.asResponseEntity(String.class)).getBody();
        assertEquals("Response entity should have body of type " + body.getClass().getName() +
                ", but actual type is " + (responseBody != null ? responseBody.getClass().getName() : "null") + ", executor: " +
                PostActionExecutor.class.getName() + ",", body, responseBody);
    }
}
