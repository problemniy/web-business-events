package com.tools.wbe.junit.core.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;

public class PostPutActionsTestProcessor implements IPostActionProcessor, IPutActionProcessor {

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
    }
}
