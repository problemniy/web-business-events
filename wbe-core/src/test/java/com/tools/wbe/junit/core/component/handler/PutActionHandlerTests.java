package com.tools.wbe.junit.core.component.handler;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.tools.wbe.junit.core.processor.PutActionTestProcessor;
import com.tools.wbe.manage.executor.PutActionExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WbeManagementConfiguration.class, PutActionHandlerTests.OwnWebBusinessEventsConfiguration.class})
public class PutActionHandlerTests {

    private static final String TEST_ACTION_1 = "TEST_1";
    private static final String TEST_ACTION_2 = "TEST_2";

    @Autowired
    private Map<String, List<Pair<IPutActionProcessor, Integer>>> putActionProcessorsMapping;
    @Autowired
    private PutActionExecutor putActionExecutor;
    @Autowired
    private WebBusinessEventsService webBusinessEventsService;

    private static final Map<IWebEventProcessor, WebEventProcessorInvolvement<IWebEventProcessor>> PROCESSORS = new HashMap<>() {{
        WebBusinessEventsService webBusinessEventsService = new WebBusinessEventsService();
        IWebEventProcessor processor_1 = Mockito.mock(PutActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> putInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.PUT), Arrays.asList(TEST_ACTION_1, TEST_ACTION_2), 14, processor_1);
        IWebEventProcessor processor_2 = Mockito.mock(PutActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> putInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.PUT), Collections.singletonList(TEST_ACTION_2), 14, processor_2);
        put(processor_1, putInvolvement_1);
        put(processor_2, putInvolvement_2);
    }};

    @Configuration
    static class OwnWebBusinessEventsConfiguration extends AWebBusinessEventsConfigAdapter {

        @Override
        protected void configure(WebBusinessEventsBuilder wbe) {
            wbe.setWebEventProcessorsInvolvements(new ArrayList<>(PROCESSORS.values()));
        }
    }

    @Test
    public void checkPutActionHandlerInvokesProcessors() {
        putActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, PutRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(null, PutResponseContext.class))
                .addAction(TEST_ACTION_1).go();
        for (Map.Entry<String, List<Pair<IPutActionProcessor, Integer>>> entry : putActionProcessorsMapping.entrySet()) {
            for (Pair<IPutActionProcessor, Integer> processorPair : entry.getValue()) {
                Mockito.verify(processorPair.getFirst(), Mockito.times(PROCESSORS.get(processorPair.getFirst()).getActions().contains(TEST_ACTION_1) ? 1 : 0))
                        .execute(any(PutRequestContext.class), any(PutResponseContext.class), any(IWebBusinessProcess.class));
            }
        }
    }
}
