package com.tools.wbe.junit.core.component.service;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.data.model.view.WbeProcessorInvolvementViewModel;
import com.tools.wbe.data.model.view.WbeProcessorViewModel;
import com.tools.wbe.junit.core.processor.GetHeadActionsTestProcessor;
import com.tools.wbe.junit.core.processor.HeadPostActionsTestProcessor;
import com.tools.wbe.junit.core.processor.PostPutActionsTestProcessor;
import com.tools.wbe.manage.component.service.WbeViewManagementService;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.util.Pair;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.ExtendedModelMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WbeManagementConfiguration.class, WbeViewManagementServiceTests.OwnWebBusinessEventsConfiguration.class})
public class WbeViewManagementServiceTests {

    private static final String TEST_ACTION_CREATE = "CREATE";
    private static final String TEST_ACTION_DELETE = "DELETE";
    private static final String TEST_ACTION_UPDATE = "UPDATE";

    private Collection<WbeProcessorInvolvementViewModel> involvementModels;

    @Autowired
    private WbeViewManagementService wbeViewManagementService;
    @Autowired
    private Map<String, List<Pair<IPostActionProcessor, Integer>>> postActionProcessorsMapping;
    @Autowired
    private Map<String, List<Pair<IPutActionProcessor, Integer>>> putActionProcessorsMapping;
    @Autowired
    private Map<String, List<Pair<IGetActionProcessor, Integer>>> getActionProcessorsMapping;
    @Autowired
    private Map<String, List<Pair<IHeadActionProcessor, Integer>>> headActionProcessorsMapping;

    private static final Map<IWebEventProcessor, WebEventProcessorInvolvement<IWebEventProcessor>> PROCESSORS = new HashMap<>() {{
        WebBusinessEventsService webBusinessEventsService = new WebBusinessEventsService();
        // create get-head processors
        IWebEventProcessor getHeadProcessor_1 = new GetHeadActionsTestProcessor();
        WebEventProcessorInvolvement<IWebEventProcessor> getHeadInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Arrays.asList(EWbeRequestMethod.GET, EWbeRequestMethod.HEAD), Arrays.asList(TEST_ACTION_CREATE, TEST_ACTION_DELETE), 34, getHeadProcessor_1);
        IWebEventProcessor getHeadProcessor_2 = new GetHeadActionsTestProcessor();
        WebEventProcessorInvolvement<IWebEventProcessor> getHeadInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Arrays.asList(EWbeRequestMethod.GET), Collections.singletonList(TEST_ACTION_UPDATE), 9, getHeadProcessor_2);
        put(getHeadProcessor_1, getHeadInvolvement_1);
        put(getHeadProcessor_2, getHeadInvolvement_2);
        // create head-post processors
        IWebEventProcessor headPostProcessor_1 = new HeadPostActionsTestProcessor();
        WebEventProcessorInvolvement<IWebEventProcessor> headPostInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Arrays.asList(EWbeRequestMethod.HEAD, EWbeRequestMethod.POST), Arrays.asList(TEST_ACTION_CREATE, TEST_ACTION_UPDATE), 27, headPostProcessor_1);
        IWebEventProcessor headPostProcessor_2 = new HeadPostActionsTestProcessor();
        WebEventProcessorInvolvement<IWebEventProcessor> headPostInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Arrays.asList(EWbeRequestMethod.HEAD, EWbeRequestMethod.POST), Collections.singletonList(TEST_ACTION_UPDATE), 87, headPostProcessor_2);
        put(headPostProcessor_1, headPostInvolvement_1);
        put(headPostProcessor_2, headPostInvolvement_2);
        // create post-put processors
        IWebEventProcessor postPutProcessor_1 = new PostPutActionsTestProcessor();
        WebEventProcessorInvolvement<IWebEventProcessor> postPutInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Arrays.asList(EWbeRequestMethod.POST, EWbeRequestMethod.PUT), Arrays.asList(TEST_ACTION_CREATE, TEST_ACTION_DELETE, TEST_ACTION_UPDATE), 27, postPutProcessor_1);
        IWebEventProcessor postPutProcessor_2 = new PostPutActionsTestProcessor();
        WebEventProcessorInvolvement<IWebEventProcessor> postPutInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.PUT), Arrays.asList(TEST_ACTION_CREATE, TEST_ACTION_DELETE, TEST_ACTION_UPDATE), 3, postPutProcessor_2);
        put(postPutProcessor_1, postPutInvolvement_1);
        put(postPutProcessor_2, postPutInvolvement_2);

    }};

    @Configuration
    static class OwnWebBusinessEventsConfiguration extends AWebBusinessEventsConfigAdapter {

        @Override
        protected void configure(WebBusinessEventsBuilder wbe) {
            wbe.setWebEventProcessorsInvolvements(new ArrayList<>(PROCESSORS.values()));
        }
    }

    @Before
    @SuppressWarnings("unchecked")
    public void executeService() {
        final String key = "result";
        ExtendedModelMap model = new ExtendedModelMap();
        wbeViewManagementService.generateMappingModels(model, key);
        involvementModels = (Collection<WbeProcessorInvolvementViewModel>) model.asMap().get(key);
        assertNotNull("Involvement models should not be null", involvementModels);
    }

    @Test
    public void checkMissingProcessorsInModel() {
        Set<String> existedProcessorsNames = new HashSet<>();
        postActionProcessorsMapping.values().forEach(pairProcessors -> existedProcessorsNames.addAll(pairProcessors.stream()
                .map(pProcessorPosition -> pProcessorPosition.getFirst().getClass().getName()).collect(Collectors.toList())));
        putActionProcessorsMapping.values().forEach(pairProcessors -> existedProcessorsNames.addAll(pairProcessors.stream()
                .map(pProcessorPosition -> pProcessorPosition.getFirst().getClass().getName()).collect(Collectors.toList())));
        getActionProcessorsMapping.values().forEach(pairProcessors -> existedProcessorsNames.addAll(pairProcessors.stream()
                .map(pProcessorPosition -> pProcessorPosition.getFirst().getClass().getName()).collect(Collectors.toList())));
        headActionProcessorsMapping.values().forEach(pairProcessors -> existedProcessorsNames.addAll(pairProcessors.stream()
                .map(pProcessorPosition -> pProcessorPosition.getFirst().getClass().getName()).collect(Collectors.toList())));
        existedProcessorsNames.forEach(processorName -> {
            boolean checked = false;
            for (WbeProcessorInvolvementViewModel involvementModel : involvementModels) {
                if (!checked) {
                    for (WbeProcessorViewModel processor : involvementModel.getProcessors()) {
                        if (processor.getName().equals(processorName)) {
                            checked = true;
                            break;
                        }
                    }
                } else break;
            }
            assertTrue(processorName + " was not found in view mapping", checked);
        });
    }

    @Test
    public void checkWrongMappedProcessorsInModel() {
        final String errorMessage = " was added to view model but it wasn't mapped to event type ";
        involvementModels.forEach(viewModel -> {
            viewModel.getProcessors().forEach(processorViewModel -> {
                if (EWbeRequestMethod.POST.name().equals(processorViewModel.getEventType())) {
                    assertTrue(processorViewModel.getName() + errorMessage + processorViewModel.getEventType(),
                            checkProcessorTypeIsValid(viewModel.getActionName(), processorViewModel.getName(), postActionProcessorsMapping));
                } else if (EWbeRequestMethod.PUT.name().equals(processorViewModel.getEventType())) {
                    assertTrue(processorViewModel.getName() + errorMessage + processorViewModel.getEventType(),
                            checkProcessorTypeIsValid(viewModel.getActionName(), processorViewModel.getName(), putActionProcessorsMapping));
                } else if (EWbeRequestMethod.GET.name().equals(processorViewModel.getEventType())) {
                    assertTrue(processorViewModel.getName() + errorMessage + processorViewModel.getEventType(),
                            checkProcessorTypeIsValid(viewModel.getActionName(), processorViewModel.getName(), getActionProcessorsMapping));
                } else if (EWbeRequestMethod.HEAD.name().equals(processorViewModel.getEventType())) {
                    assertTrue(processorViewModel.getName() + errorMessage + processorViewModel.getEventType(),
                            checkProcessorTypeIsValid(viewModel.getActionName(), processorViewModel.getName(), headActionProcessorsMapping));
                }
            });
        });
    }

    private <PRO extends IWebEventProcessor> boolean checkProcessorTypeIsValid(String actionName, String processorName, Map<String, List<Pair<PRO, Integer>>> processorsMapping) {
        List<Pair<PRO, Integer>> pairProcessorPosition = processorsMapping.get(actionName);
        if (pairProcessorPosition != null) {
            for (Pair<PRO, Integer> proIntegerPair : pairProcessorPosition) {
                if (proIntegerPair.getFirst().getClass().getName().equals(processorName)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Test
    public void checkPositionOfProcessorsInModelIsValid() {
        involvementModels.forEach(viewModel -> {
            Map<String, List<Integer>> logMap = new HashMap<>();
            viewModel.getProcessors().forEach(processor -> {
                List<Integer> orders = logMap.computeIfAbsent(processor.getEventType(), k -> new ArrayList<>());
                orders.add(processor.getOrder());
            });
            assertThat("There is a problem with processors ordering while view mapping models generation; test action = [" + viewModel.getActionName() + "], orders: "
                    + logMap.toString(), viewModel.getProcessors(), areProcessorsInAscendingOrdering());
        });
    }

    private Matcher<? super List<WbeProcessorViewModel>> areProcessorsInAscendingOrdering() {
        return new TypeSafeMatcher<>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Processors in view model should be in descending ordering");
            }

            @Override
            protected boolean matchesSafely(List<WbeProcessorViewModel> item) {
                for (int i = 0; i < item.size() - 1; i++) {
                    if (item.get(i).getOrder() > item.get(i + 1).getOrder() && item.get(i).getEventType().equals(item.get(i + 1).getEventType())) {
                        return false;
                    }
                }
                return true;
            }
        };
    }
}
