package com.tools.wbe.junit.core.component.executor;

import com.tools.wbe.core.component.handler.GetActionHandler;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.business.event.GetAction;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.junit.core.service.IWebEventExecutorTestService;
import com.tools.wbe.junit.core.service.WebEventExecutorTestService;
import com.tools.wbe.manage.executor.GetActionExecutor;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WbeManagementConfiguration.class, GetActionExecutorTests.OwnWebBusinessEventsConfiguration.class})
public class GetActionExecutorTests {

    private static final String TEST_GET_ACTION_EXECUTOR_SCOPE_REQUEST = "/wbe/tests/testGetActionExecutorScope";

    @Autowired
    private GetActionExecutor getActionExecutor;
    @Autowired
    private GetActionHandler getActionHandler;
    @Autowired
    private WebBusinessEventsService webBusinessEventsService;
    @Autowired
    private IWebEventExecutorTestService webEventExecutorTestService;
    @Autowired
    private WebApplicationContext context;

    @Configuration
    @ComponentScan("com.tools.wbe.junit.core.controller")
    static class OwnWebBusinessEventsConfiguration {

        @Autowired
        private WebBusinessEventsService webBusinessEventsService;

        @Bean
        public WebEventExecutorTestService webEventExecutorTestService() {
            return new WebEventExecutorTestService(webBusinessEventsService);
        }

        @Bean
        @Primary
        public GetActionHandler getActionHandler() {
            return Mockito.mock(GetActionHandler.class);
        }
    }

    @Test
    public void checkContextActionIsCalled() {
        webEventExecutorTestService.checkContextActionIsCalled(getActionHandler, getActionExecutor, GetRequestContext.class, GetResponseContext.class);
    }

    @Test
    public void checkExecutorActionsAreCalled() {
        webEventExecutorTestService.checkExecutorActionsAreCalled(getActionHandler, getActionExecutor, GetRequestContext.class, GetResponseContext.class);
    }

    @Test
    public void checkContextAndExecutorActionsAreCalled() {
        webEventExecutorTestService.checkContextAndExecutorActionsAreCalled(getActionHandler, getActionExecutor, GetRequestContext.class, GetResponseContext.class);
    }

    @Test
    public void checkExecutorHasEmptyFieldsOnNewRequest() throws Exception {
        webEventExecutorTestService.checkExecutorHasEmptyFieldsOnNewRequest(TEST_GET_ACTION_EXECUTOR_SCOPE_REQUEST, context);
    }

    @Test
    public void checkExecutorFillEventProperties() {
        webEventExecutorTestService.checkExecutorFillEventProperties(getActionHandler, getActionExecutor, GetAction.class, GetRequestContext.class, GetResponseContext.class);
    }

    @Test
    public void checkExecutorsGoHasValidResponseObjects() {
        String body = "body";
        AWebEventExecutor<GetAction, GetRequestContext, GetResponseContext>.Result result = getActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, null, "action", GetRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(null, null, body, GetResponseContext.class))
                .go();
        assertNotNull("Expected not null for " + HttpHeaders.class.getName() + " after completion of "
                + GetActionExecutor.class.getName(), result.asHeaders());
        assertNotNull("Expected not null for " + GetResponseContext.class.getName() + " after completion of "
                + GetActionExecutor.class.getName(), result.asResponseContext());
        assertNotNull("Expected not null for " + ResponseEntity.class.getName() + "<" + Serializable.class.getName() + "> after completion of "
                + GetActionExecutor.class.getName(), result.asResponseEntity());
        assertNotNull("Expected not null for " + ResponseEntity.class.getName() + "<" + String.class.getName() + "> after completion of "
                + GetActionExecutor.class.getName(), result.asResponseEntity(String.class));
        String resultBody = Objects.requireNonNull(result.asResponseEntity(String.class)).getBody();
        assertEquals("Response entity should have body of type " + body.getClass().getName() +
                ", but actual type is " + (resultBody != null ? resultBody.getClass().getName() : "null") + ", executor: " +
                GetActionExecutor.class.getName() + ",", body, resultBody);
    }
}
