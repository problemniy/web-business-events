package com.tools.wbe.junit.core.config;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeBasicConfiguration;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.junit.core.processor.GetActionTestProcessor;
import com.tools.wbe.junit.core.processor.HeadActionTestProcessor;
import com.tools.wbe.junit.core.processor.PostActionTestProcessor;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.util.Pair;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WbeManagementConfiguration.class)
public class WbeBasicConfigurationTests {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private static final String TEST_ACTION_1 = "TEST_1";
    private static final String TEST_ACTION_2 = "TEST_2";
    private static final String TEST_ACTION_3 = "TEST_3";
    private static Map<IWebEventProcessor, WebEventProcessorInvolvement<IWebEventProcessor>> VALID_PROCESSORS_INVOLVEMENTS = new HashMap<>() {{
        WebBusinessEventsService webBusinessEventsService = new WebBusinessEventsService();
        // head involvements
        IWebEventProcessor headProcessor_1 = Mockito.mock(HeadActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> headInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.HEAD), Arrays.asList(TEST_ACTION_1, TEST_ACTION_2), 3,
                headProcessor_1);
        IWebEventProcessor headProcessor_2 = Mockito.mock(HeadActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> headInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.HEAD), Collections.singletonList(TEST_ACTION_2), 11,
                headProcessor_2);
        //post involvements
        IWebEventProcessor postProcessor_1 = Mockito.mock(PostActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> postInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.POST), Collections.singletonList(TEST_ACTION_2), 3,
                postProcessor_1);
        IWebEventProcessor postProcessor_2 = Mockito.mock(PostActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> postInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.POST), Arrays.asList(TEST_ACTION_2, TEST_ACTION_3), 6,
                postProcessor_2);
        IWebEventProcessor postProcessor_3 = Mockito.mock(PostActionTestProcessor.class);
        WebEventProcessorInvolvement<IWebEventProcessor> postInvolvement_3 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.POST), Arrays.asList(TEST_ACTION_1, TEST_ACTION_2, TEST_ACTION_3), 0,
                postProcessor_3);
        // fill collection
        put(headProcessor_1, headInvolvement_1);
        put(headProcessor_2, headInvolvement_2);
        put(postProcessor_1, postInvolvement_1);
        put(postProcessor_2, postInvolvement_2);
        put(postProcessor_3, postInvolvement_3);
    }};
    private static List<WebEventProcessorInvolvement<IWebEventProcessor>> WRONG_INVOLVEMENTS = new ArrayList<>() {{
        WebBusinessEventsService webBusinessEventsService = new WebBusinessEventsService();
        // head involvements
        WebEventProcessorInvolvement<IWebEventProcessor> headInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.HEAD), Arrays.asList(TEST_ACTION_1, TEST_ACTION_2), 3,
                Mockito.mock(HeadActionTestProcessor.class));
        WebEventProcessorInvolvement<IWebEventProcessor> headInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.HEAD), Collections.singletonList(TEST_ACTION_2), 11,
                Mockito.mock(HeadActionTestProcessor.class));
        //get involvements (1 wrong)
        WebEventProcessorInvolvement<IWebEventProcessor> getInvolvement_1 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.GET), Collections.singletonList(TEST_ACTION_1), 15,
                Mockito.mock(GetActionTestProcessor.class));
        WebEventProcessorInvolvement<IWebEventProcessor> getInvolvement_2 = webBusinessEventsService.createWebEventProcessorInvolvement(
                Collections.singletonList(EWbeRequestMethod.POST), Arrays.asList(TEST_ACTION_2, TEST_ACTION_3), 4,
                Mockito.mock(GetActionTestProcessor.class));
        // fill collection
        add(headInvolvement_1);
        add(headInvolvement_2);
        add(getInvolvement_1);
        add(getInvolvement_2);
    }};

    @Autowired
    private ApplicationContext context;

    @Autowired
    @Qualifier("wbePostActionProcessorsMapping")
    Map<String, List<Pair<IPostActionProcessor, Integer>>> postActionProcessorsMapping;

    @Autowired
    @Qualifier("wbeGetActionProcessorsMapping")
    Map<String, List<Pair<IGetActionProcessor, Integer>>> getActionProcessorsMapping;

    @Autowired
    @Qualifier("wbeHeadActionProcessorsMapping")
    Map<String, List<Pair<IHeadActionProcessor, Integer>>> headActionProcessorsMapping;

    @Configuration
    static class OwnWebBusinessEventsConfiguration extends AWebBusinessEventsConfigAdapter {

        @Override
        protected void configure(WebBusinessEventsBuilder wbe) {
            wbe.setWebEventProcessorsInvolvements(new ArrayList<>(VALID_PROCESSORS_INVOLVEMENTS.values()));
        }
    }

    @Test
    public void checkWrongProcessorTypeInInvolvement() {
        exception.expect(IllegalArgumentException.class);
        exception.reportMissingExceptionWithMessage("Wrong processors types in mapping should throw " + IllegalArgumentException.class.getName());
        AWebBusinessEventsConfigAdapter ownWbeConfigAdapter = new AWebBusinessEventsConfigAdapter() {
            @Override
            protected void configure(WebBusinessEventsBuilder wbe) {
                wbe.setWebEventProcessorsInvolvements(WRONG_INVOLVEMENTS);
            }
        };
        ownWbeConfigAdapter.afterPropertiesSet();
        WbeBasicConfiguration wrongWbeConfiguration = new WbeBasicConfiguration(context, ownWbeConfigAdapter);
        wrongWbeConfiguration.postActionProcessorsMapping();
        wrongWbeConfiguration.getActionProcessorsMapping();
        wrongWbeConfiguration.headActionProcessorsMapping();
    }

    @Test
    public void checkPostActionProcessorsMapping() {
        checkActionProcessorsMapping(postActionProcessorsMapping, "wbePostActionProcessorsMapping");
    }

    @Test
    public void checkGetActionProcessorsMapping() {
        checkActionProcessorsMapping(getActionProcessorsMapping, "wbeGetActionProcessorsMapping");
    }

    @Test
    public void checkHeadActionProcessorsMapping() {
        checkActionProcessorsMapping(headActionProcessorsMapping, "wbeHeadActionProcessorsMapping");
    }

    private <T extends IWebEventProcessor> void checkActionProcessorsMapping(Map<String, List<Pair<T, Integer>>> processorsMapping, String beanName) {
        for (Map.Entry<String, List<Pair<T, Integer>>> entry : processorsMapping.entrySet()) {
            String actionName = entry.getKey();
            WebEventProcessorInvolvement<IWebEventProcessor> previousInvolvement = null;
            for (Pair<T, Integer> pro : entry.getValue()) {
                WebEventProcessorInvolvement<IWebEventProcessor> involvement = VALID_PROCESSORS_INVOLVEMENTS.get(pro.getFirst());
                assertThat(attachProblemBeanName(beanName, "Processor involvement should have the same action with action of mapping (\'" +
                                actionName + "\'), but it doesn't contain it: " + involvement.getActions().toString()),
                        involvement.getActions(), hasItem(actionName));

                if (previousInvolvement != null) {
                    assertTrue(attachProblemBeanName(beanName, "Collection of processors involvements " +
                                    "should contain a sorted list in ascending order: failed order - " + previousInvolvement.getOrder() +
                                    ", next order - " + involvement.getOrder() + " (" + previousInvolvement.getOrder() + " > " + involvement.getOrder() + ")"),
                            previousInvolvement.getOrder() <= involvement.getOrder());
                }
                previousInvolvement = involvement;
            }
        }
    }

    private String attachProblemBeanName(String beanName, String message) {
        return "There is problem with bean \'" + beanName + "\'. Reason: " + message;
    }
}
