package com.tools.wbe.junit.core.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;

public class HeadActionTestProcessor implements IHeadActionProcessor {

    @Override
    public void execute(HeadRequestContext requestContext, HeadResponseContext responseContext, IWebBusinessProcess businessProcess) {
    }
}
