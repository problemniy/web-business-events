package com.tools.wbe.junit.core.component.executor;

import com.tools.wbe.core.component.handler.PutActionHandler;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.business.event.PutAction;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.tools.wbe.junit.core.service.IWebEventExecutorTestService;
import com.tools.wbe.junit.core.service.WebEventExecutorTestService;
import com.tools.wbe.manage.executor.PostActionExecutor;
import com.tools.wbe.manage.executor.PutActionExecutor;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WbeManagementConfiguration.class, PutActionExecutorTests.OwnWebBusinessEventsConfiguration.class})
public class PutActionExecutorTests {

    private static final String TEST_PUT_ACTION_EXECUTOR_SCOPE_REQUEST = "/wbe/tests/testPutActionExecutorScope";

    @Autowired
    private PutActionExecutor putActionExecutor;
    @Autowired
    private PutActionHandler putActionHandler;
    @Autowired
    private WebBusinessEventsService webBusinessEventsService;
    @Autowired
    private IWebEventExecutorTestService webEventExecutorTestService;
    @Autowired
    private WebApplicationContext context;

    @Configuration
    @ComponentScan("com.tools.wbe.junit.core.controller")
    static class OwnWebBusinessEventsConfiguration {

        @Autowired
        private WebBusinessEventsService webBusinessEventsService;

        @Bean
        public WebEventExecutorTestService webEventExecutorTestService() {
            return new WebEventExecutorTestService(webBusinessEventsService);
        }

        @Bean
        @Primary
        public PutActionHandler putActionHandler() {
            return Mockito.mock(PutActionHandler.class);
        }
    }

    @Test
    public void checkContextActionIsCalled() {
        webEventExecutorTestService.checkContextActionIsCalled(putActionHandler, putActionExecutor, PutRequestContext.class, PutResponseContext.class);
    }

    @Test
    public void checkExecutorActionsAreCalled() {
        webEventExecutorTestService.checkExecutorActionsAreCalled(putActionHandler, putActionExecutor, PutRequestContext.class, PutResponseContext.class);
    }

    @Test
    public void checkContextAndExecutorActionsAreCalled() {
        webEventExecutorTestService.checkContextAndExecutorActionsAreCalled(putActionHandler, putActionExecutor, PutRequestContext.class, PutResponseContext.class);
    }

    @Test
    public void checkExecutorHasEmptyFieldsOnNewRequest() throws Exception {
        webEventExecutorTestService.checkExecutorHasEmptyFieldsOnNewRequest(TEST_PUT_ACTION_EXECUTOR_SCOPE_REQUEST, context);
    }

    @Test
    public void checkExecutorFillEventProperties() {
        webEventExecutorTestService.checkExecutorFillEventProperties(putActionHandler, putActionExecutor, PutAction.class, PutRequestContext.class, PutResponseContext.class);
    }

    @Test
    public void checkExecutorsGoHasValidResponseObjects() {
        String body = "body";
        AWebEventExecutor<PutAction, PutRequestContext, PutResponseContext>.Result result = putActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, body, "action", PutRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(null, null, body, PutResponseContext.class))
                .go();
        assertNotNull("Expected not null for " + HttpHeaders.class.getName() + " after completion of "
                + PostActionExecutor.class.getName(), result.asHeaders());
        assertNotNull("Expected not null for " + GetResponseContext.class.getName() + " after completion of "
                + PostActionExecutor.class.getName(), result.asResponseContext());
        assertNotNull("Expected not null for " + ResponseEntity.class.getName() + "<" + Serializable.class.getName() + "> after completion of "
                + PostActionExecutor.class.getName(), result.asResponseEntity());
        assertNotNull("Expected not null for " + ResponseEntity.class.getName() + "<" + String.class.getName() + "> after completion of "
                + PostActionExecutor.class.getName(), result.asResponseEntity(String.class));
        String requestBody = Objects.requireNonNull(result.asResponseEntity(String.class)).getBody();
        assertEquals("Request entity should have body of type " + body.getClass().getName() +
                ", but actual type is " + (requestBody != null ? requestBody.getClass().getName() : "null") + ", executor: " +
                PostActionExecutor.class.getName() + ",", body, requestBody);
        String responseBody = Objects.requireNonNull(result.asResponseEntity(String.class)).getBody();
        assertEquals("Response entity should have body of type " + body.getClass().getName() +
                ", but actual type is " + (responseBody != null ? responseBody.getClass().getName() : "null") + ", executor: " +
                PostActionExecutor.class.getName() + ",", body, responseBody);
    }
}
