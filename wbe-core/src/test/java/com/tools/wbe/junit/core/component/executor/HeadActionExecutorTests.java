package com.tools.wbe.junit.core.component.executor;

import com.tools.wbe.core.component.handler.HeadActionHandler;
import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.business.event.HeadAction;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;
import com.tools.wbe.junit.core.service.IWebEventExecutorTestService;
import com.tools.wbe.junit.core.service.WebEventExecutorTestService;
import com.tools.wbe.manage.executor.HeadActionExecutor;
import com.tools.wbe.manage.executor.abs.AWebEventExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;

import static org.junit.Assert.assertNull;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WbeManagementConfiguration.class, HeadActionExecutorTests.OwnWebBusinessEventsConfiguration.class})
public class HeadActionExecutorTests {

    private static final String TEST_HEAD_ACTION_EXECUTOR_SCOPE_REQUEST = "/wbe/tests/testHeadActionExecutorScope";

    @Autowired
    private HeadActionExecutor headActionExecutor;
    @Autowired
    private HeadActionHandler headActionHandler;
    @Autowired
    private WebBusinessEventsService webBusinessEventsService;
    @Autowired
    private IWebEventExecutorTestService webEventExecutorTestService;
    @Autowired
    private WebApplicationContext context;

    @Configuration
    @ComponentScan("com.tools.wbe.junit.core.controller")
    static class OwnWebBusinessEventsConfiguration {

        @Autowired
        private WebBusinessEventsService webBusinessEventsService;

        @Bean
        public WebEventExecutorTestService webEventExecutorTestService() {
            return new WebEventExecutorTestService(webBusinessEventsService);
        }

        @Bean
        @Primary
        public HeadActionHandler headActionHandler() {
            return Mockito.mock(HeadActionHandler.class);
        }
    }

    @Test
    public void checkContextActionIsCalled() {
        webEventExecutorTestService.checkContextActionIsCalled(headActionHandler, headActionExecutor, HeadRequestContext.class, HeadResponseContext.class);
    }

    @Test
    public void checkExecutorActionsAreCalled() {
        webEventExecutorTestService.checkExecutorActionsAreCalled(headActionHandler, headActionExecutor, HeadRequestContext.class, HeadResponseContext.class);
    }

    @Test
    public void checkContextAndExecutorActionsAreCalled() {
        webEventExecutorTestService.checkContextAndExecutorActionsAreCalled(headActionHandler, headActionExecutor, HeadRequestContext.class, HeadResponseContext.class);
    }

    @Test
    public void checkExecutorHasEmptyFieldsOnNewRequest() throws Exception {
        webEventExecutorTestService.checkExecutorHasEmptyFieldsOnNewRequest(TEST_HEAD_ACTION_EXECUTOR_SCOPE_REQUEST, context);
    }

    @Test
    public void checkExecutorFillEventProperties() {
        webEventExecutorTestService.checkExecutorFillEventProperties(headActionHandler, headActionExecutor, HeadAction.class, HeadRequestContext.class, HeadResponseContext.class);
    }

    @Test
    public void checkExecutorsGoHasValidResponseObjects() {
        AWebEventExecutor<HeadAction, HeadRequestContext, HeadResponseContext>.Result result = headActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(new HttpHeaders(), null, null, "action", HeadRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(null, null, "body", HeadResponseContext.class))
                .go();
        assertNotNull("Expected not null for " + HttpHeaders.class.getName() + " after completion of "
                + HeadActionExecutor.class.getName(), result.asHeaders());
        assertNotNull("Expected not null for " + GetResponseContext.class.getName() + " after completion of "
                + HeadActionExecutor.class.getName(), result.asResponseContext());
        assertNull("Expected null for " + ResponseEntity.class.getName() + "<" + Serializable.class.getName() + "> after completion of "
                + HeadActionExecutor.class.getName(), result.asResponseEntity());
        assertNull("Expected null for " + ResponseEntity.class.getName() + "<" + String.class.getName() + "> after completion of "
                + HeadActionExecutor.class.getName(), result.asResponseEntity(String.class));
    }
}
