<#import "/spring.ftl" as spring />
<div class="wbe-processors">
    <#list mappingModels as viewInvolvement>
        <table>
            <#assign previousEventType = "">
            <#list viewInvolvement.processors as viewProcessor>
                <#if previousEventType != viewProcessor.eventType>
                    <tr class="wbe-processors-title">
                        <#if previousEventType = "">
                            <th class="wbe-action-name-title">${viewInvolvement.actionName}</th>
                        <#else>
                            <th class="wbe-action-name-title"></th>
                        </#if>

                        <th class="wbe-event-type-title">Type:</th>
                        <th class="wbe-processors-col3">${viewProcessor.eventType}</th>
                    </tr>
                    <tr class="wbe-processors-container">
                        <th class="wbe-table-border wbe-processors-col1">Order</th>
                        <th class="wbe-table-border wbe-processors-col2">Processor</th>
                        <th class="wbe-table-border wbe-processors-col3"></th>
                    </tr>
                </#if>
                <tr class="wbe-processors-container">
                    <td class="wbe-table-border wbe-processors-col1">${viewProcessor.order}</td>
                    <td class="wbe-table-border wbe-processors-col2">${viewProcessor.name}</td>
                    <td class="wbe-table-border wbe-processors-col3">${viewProcessor.eventType}</td>
                    <#assign previousEventType = viewProcessor.eventType>
                </tr>
            </#list>
        </table>
    </#list>
</div>
