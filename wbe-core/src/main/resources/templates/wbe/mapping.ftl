<#import "/spring.ftl" as spring />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="<@spring.url '/wbe/css/wbe.css'/>">
    <title>Web Business Events</title>
</head>
<body>
<div class="wbe-mapping-title">Welcome to <span class="wbe-mapping-tool-name">Web Business Events Mapping</span>! Next
    processors were mapped to actions:
</div>
<#include "processors.ftl">
</body>
</html>
