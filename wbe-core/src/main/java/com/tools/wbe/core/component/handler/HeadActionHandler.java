package com.tools.wbe.core.component.handler;

import com.tools.wbe.core.component.handler.abs.AWebEventHandler;
import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.event.HeadAction;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class HeadActionHandler extends AWebEventHandler<HeadAction, HeadRequestContext, HeadResponseContext, IHeadActionProcessor> {

    private final Map<String, List<Pair<IHeadActionProcessor, Integer>>> headActionProcessorsMapping;

    @Autowired
    public HeadActionHandler(@Qualifier("wbeHeadActionProcessorsMapping") Map<String, List<Pair<IHeadActionProcessor, Integer>>> headActionProcessorsMapping) {
        super(HeadAction.class);
        this.headActionProcessorsMapping = headActionProcessorsMapping;
    }

    @Override
    protected Map<String, List<Pair<IHeadActionProcessor, Integer>>> getProcessorsMapping() {
        return headActionProcessorsMapping;
    }

    @Override
    protected void nextProcessor(Pair<IHeadActionProcessor, Integer> pProcessorPosition, HeadAction headAction, IWebBusinessProcess businessProcess) {
        pProcessorPosition.getFirst().execute(headAction.getRequestContext(), headAction.getResponseContext(), businessProcess);
    }
}
