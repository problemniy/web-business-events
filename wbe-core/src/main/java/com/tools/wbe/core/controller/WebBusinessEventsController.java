package com.tools.wbe.core.controller;

import com.tools.wbe.manage.component.service.IWbeViewManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/wbe")
public class WebBusinessEventsController {

    private final IWbeViewManagementService wbeViewManagementService;

    @Autowired
    public WebBusinessEventsController(IWbeViewManagementService wbeViewManagementService) {
        this.wbeViewManagementService = wbeViewManagementService;
    }

    @RequestMapping(value = "/mapping", method = RequestMethod.GET)
    public String mapping(Model model) {
        wbeViewManagementService.generateMappingModels(model, "mappingModels");
        return "wbe/mapping";
    }
}
