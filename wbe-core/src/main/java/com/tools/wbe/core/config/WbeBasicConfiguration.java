package com.tools.wbe.core.config;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.manage.executor.GetActionExecutor;
import com.tools.wbe.manage.executor.HeadActionExecutor;
import com.tools.wbe.manage.executor.PostActionExecutor;
import com.tools.wbe.manage.executor.PutActionExecutor;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.util.Pair;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class WbeBasicConfiguration {

    private final ApplicationContext context;
    private final WebBusinessEventsBuilder wbeBuilder;

    @Autowired
    public WbeBasicConfiguration(ApplicationContext context, AWebBusinessEventsConfigAdapter webBusinessEventsConfigAdapter) {
        this.context = context;
        wbeBuilder = webBusinessEventsConfigAdapter.getWbeBuilder();
    }

    @Bean
    public ViewResolver viewResolver() {
        FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver();
        viewResolver.setExposeSpringMacroHelpers(true);
        viewResolver.setExposeRequestAttributes(true);
        viewResolver.setPrefix("");
        viewResolver.setSuffix(".ftl");
        viewResolver.setContentType("text/html;charset=UTF-8");
        return viewResolver;
    }

    @Bean
    public FreeMarkerConfigurer freemarkerConfig() throws IOException, TemplateException {
        FreeMarkerConfigurationFactory factory = new FreeMarkerConfigurationFactory();
        factory.setTemplateLoaderPath("classpath:/templates");
        factory.setDefaultEncoding("UTF-8");
        ClassTemplateLoader classTemplateLoader = new ClassTemplateLoader(context.getClassLoader(), "/templates");
        ClassTemplateLoader baseClassTemplateLoader = new ClassTemplateLoader(FreeMarkerConfigurer.class, "");
        MultiTemplateLoader multiTemplateLoader = new MultiTemplateLoader(new TemplateLoader[]{classTemplateLoader, baseClassTemplateLoader});
        factory.setPostTemplateLoaders(multiTemplateLoader);
        FreeMarkerConfigurer result = new FreeMarkerConfigurer();
        result.setConfiguration(factory.createConfiguration());
        return result;
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public PostActionExecutor postActionExecutor() {
        return new PostActionExecutor();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public PutActionExecutor putActionExecutor() {
        return new PutActionExecutor();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public GetActionExecutor getActionExecutor() {
        return new GetActionExecutor();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public HeadActionExecutor headActionExecutor() {
        return new HeadActionExecutor();
    }

    @Bean("wbePostActionProcessorsMapping")
    public Map<String, List<Pair<IPostActionProcessor, Integer>>> postActionProcessorsMapping() {
        return getProcessorsMapping(IPostActionProcessor.class, EWbeRequestMethod.POST);
    }

    @Bean("wbePutActionProcessorsMapping")
    public Map<String, List<Pair<IPutActionProcessor, Integer>>> putActionProcessorsMapping() {
        return getProcessorsMapping(IPutActionProcessor.class, EWbeRequestMethod.PUT);
    }

    @Bean("wbeGetActionProcessorsMapping")
    public Map<String, List<Pair<IGetActionProcessor, Integer>>> getActionProcessorsMapping() {
        return getProcessorsMapping(IGetActionProcessor.class, EWbeRequestMethod.GET);
    }

    @Bean("wbeHeadActionProcessorsMapping")
    public Map<String, List<Pair<IHeadActionProcessor, Integer>>> headActionProcessorsMapping() {
        return getProcessorsMapping(IHeadActionProcessor.class, EWbeRequestMethod.HEAD);
    }

    private <A extends IWebEventProcessor> Map<String, List<Pair<A, Integer>>> getProcessorsMapping(Class<A> processorType, EWbeRequestMethod filterByRequestMethod) {
        final Map<String, List<Pair<A, Integer>>> resultMap = new HashMap<>();
        Comparator<Pair<A, Integer>> processorsSortingComparator = Comparator.comparingInt(Pair::getSecond);
        wbeBuilder.getWebEventProcessorsInvolvements().forEach(processorInvolvement -> {
            if (!processorInvolvement.getEventTypes().contains(filterByRequestMethod)) {
                return;
            }
            Assert.isTrue(processorType.isInstance(processorInvolvement.getProcessor()),
                    "Wrong type of processor was found in processor involvement with order " + processorInvolvement.getOrder() +
                            ". Type " + processorInvolvement.getProcessor().getClass() + " is not inherited by expected type " + processorType.getName());
            for (String actionName : processorInvolvement.getActions()) {
                A processor = processorType.cast(processorInvolvement.getProcessor());
                List<Pair<A, Integer>> registeredProcessors = resultMap.get(actionName);
                if (registeredProcessors == null) {
                    List<Pair<A, Integer>> processorsNewList = new ArrayList<>();
                    processorsNewList.add(Pair.of(processor, processorInvolvement.getOrder()));
                    resultMap.put(actionName, processorsNewList);
                } else {
                    registeredProcessors.add(Pair.of(processor, processorInvolvement.getOrder()));
                }
            }
        });
        resultMap.forEach((actionName, eventProcessors) -> eventProcessors.sort(processorsSortingComparator));
        return resultMap;
    }
}
