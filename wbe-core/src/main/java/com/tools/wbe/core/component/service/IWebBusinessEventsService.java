package com.tools.wbe.core.component.service;

import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.List;

public interface IWebBusinessEventsService {

    <T extends AWebRequestContext> T prepareRequestContext(HttpHeaders headers, HttpServletRequest request, Class<T> contextType);

    <T extends AWebRequestContext> T prepareRequestContext(HttpHeaders headers, HttpServletRequest request, @Nullable Serializable body, Class<T> contextType);

    <T extends AWebRequestContext> T prepareRequestContext(HttpHeaders headers, HttpServletRequest request, @Nullable Serializable body, @Nullable String actionName, Class<T> contextType);

    <T extends AWebResponseContext> T prepareResponseContext(HttpServletResponse response, Class<T> contextType);

    <T extends AWebResponseContext> T prepareResponseContext(HttpHeaders headers, HttpServletResponse response, Class<T> contextType);

    <T extends AWebResponseContext> T prepareResponseContext(HttpHeaders headers, HttpServletResponse response, @Nullable Serializable body, Class<T> contextType);

    <T extends IWebEventProcessor> WebEventProcessorInvolvement<T> createWebEventProcessorInvolvement(List<EWbeRequestMethod> eventTypes, List<String> actions, int order, T processor);
}
