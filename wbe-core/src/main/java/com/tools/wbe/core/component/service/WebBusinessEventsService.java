package com.tools.wbe.core.component.service;

import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.common.spec.EWbeRequestMethod;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Service
public class WebBusinessEventsService implements IWebBusinessEventsService {

    private static final String ILLEGAL_CLASS_TYPE_ERROR_MSG = "Provided type of class isn't supported by this service because it doesn't have suitable constructor";

    @Override
    public <T extends AWebRequestContext> T prepareRequestContext(HttpHeaders headers, HttpServletRequest request, Class<T> contextType) {
        return prepareRequestContext(headers, request, null, null, contextType);
    }

    @Override
    public <T extends AWebRequestContext> T prepareRequestContext(HttpHeaders headers, HttpServletRequest request, @Nullable Serializable body, Class<T> contextType) {
        return prepareRequestContext(headers, request, body, null, contextType);
    }

    @Override
    public <T extends AWebRequestContext> T prepareRequestContext(HttpHeaders headers, HttpServletRequest request, @Nullable Serializable body, @Nullable String actionName, Class<T> contextType) {
        return createRequestContextViaProvidedClass(headers, request, body, actionName, contextType);
    }

    @Override
    public <T extends AWebResponseContext> T prepareResponseContext(HttpServletResponse response, Class<T> contextType) {
        return prepareResponseContext(new HttpHeaders(), response, contextType);
    }

    @Override
    public <T extends AWebResponseContext> T prepareResponseContext(HttpHeaders headers, HttpServletResponse response, Class<T> contextType) {
        return prepareResponseContext(headers, response, null, contextType);
    }

    @Override
    public <T extends AWebResponseContext> T prepareResponseContext(HttpHeaders headers, HttpServletResponse response, @Nullable Serializable body, Class<T> contextType) {
        return createResponseContextViaProvidedClass(headers, response, body, contextType);
    }

    @Override
    public <T extends IWebEventProcessor> WebEventProcessorInvolvement<T> createWebEventProcessorInvolvement(List<EWbeRequestMethod> eventTypes, List<String> actions, int order, T processor) {
        WebEventProcessorInvolvement<T> involvement = new WebEventProcessorInvolvement<>();
        involvement.setEventTypes(eventTypes);
        involvement.setActions(actions);
        involvement.setOrder(order);
        involvement.setProcessor(processor);
        return involvement;
    }

    private <T extends AWebRequestContext> T createRequestContextViaProvidedClass(HttpHeaders headers, HttpServletRequest request, @Nullable Serializable body, @Nullable String actionName, Class<T> contextType) {
        try {
            T context = null;
            Object[] params = new Object[4];
            Object[] possibleParams = new Object[3];
            Object[] mandatoryParams = new Object[2];
            Constructor possibleConstructor = null;
            Constructor mandatoryConstructor = null;
            Constructor[] allConstructors = contextType.getDeclaredConstructors();
            for (Constructor constructor : allConstructors) {
                Class<?>[] pType = constructor.getParameterTypes();
                if (pType.length == 4) {
                    int equals = 0;
                    for (int i = 0; i < pType.length; i++) {
                        if (HttpHeaders.class.equals(pType[i])) {
                            params[i] = headers;
                            equals++;
                        } else if (HttpServletRequest.class.equals(pType[i])) {
                            params[i] = request;
                            equals++;
                        } else if (String.class.equals(pType[i])) {
                            params[i] = actionName;
                            equals++;
                        } else if (Serializable.class.equals(pType[i])) {
                            params[i] = body;
                            equals++;
                        }
                    }
                    if (equals == pType.length) {
                        context = contextType.cast(constructor.newInstance(params));
                        break;
                    }
                } else if (possibleConstructor == null && pType.length == 3) {
                    int equals = 0;
                    for (int i = 0; i < pType.length; i++) {
                        if (HttpHeaders.class.equals(pType[i])) {
                            possibleParams[i] = headers;
                            equals++;
                        } else if (HttpServletRequest.class.equals(pType[i])) {
                            possibleParams[i] = request;
                            equals++;
                        } else if (actionName != null && String.class.equals(pType[i])) {
                            possibleParams[i] = actionName;
                            equals++;
                        } else if (actionName == null && Serializable.class.equals(pType[i])) {
                            possibleParams[i] = body;
                            equals++;
                        }
                    }
                    if (equals == pType.length) {
                        possibleConstructor = constructor;
                    }
                } else if (mandatoryConstructor == null && pType.length == 2) {
                    int equals = 0;
                    for (int i = 0; i < pType.length; i++) {
                        if (HttpHeaders.class.equals(pType[i])) {
                            mandatoryParams[i] = headers;
                            equals++;
                        } else if (HttpServletRequest.class.equals(pType[i])) {
                            mandatoryParams[i] = request;
                            equals++;
                        }
                    }
                    if (equals == pType.length) {
                        mandatoryConstructor = constructor;
                    }
                }
            }
            if (context == null && possibleConstructor != null) {
                context = contextType.cast(possibleConstructor.newInstance(possibleParams));
            } else if (context == null && mandatoryConstructor != null) {
                context = contextType.cast(mandatoryConstructor.newInstance(mandatoryParams));
            } else if (context == null) {
                throw new IllegalArgumentException(ILLEGAL_CLASS_TYPE_ERROR_MSG);
            }
            return context;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(ILLEGAL_CLASS_TYPE_ERROR_MSG);
        }
    }

    private <T extends AWebResponseContext> T createResponseContextViaProvidedClass(HttpHeaders headers, HttpServletResponse response, @Nullable Serializable body, Class<T> contextType) {
        try {
            T context = null;
            Object[] params = new Object[3];
            Object[] mandatoryParams = new Object[2];
            Constructor mandatoryConstructor = null;
            Constructor[] allConstructors = contextType.getDeclaredConstructors();
            for (Constructor constructor : allConstructors) {
                Class<?>[] pType = constructor.getParameterTypes();
                if (pType.length == 3) {
                    int equals = 0;
                    for (int i = 0; i < pType.length; i++) {
                        if (HttpHeaders.class.equals(pType[i])) {
                            params[i] = headers;
                            equals++;
                        } else if (HttpServletResponse.class.equals(pType[i])) {
                            params[i] = response;
                            equals++;
                        } else if (Serializable.class.equals(pType[i])) {
                            params[i] = body;
                            equals++;
                        }
                    }
                    if (equals == pType.length) {
                        context = contextType.cast(constructor.newInstance(params));
                        break;
                    }
                } else if (mandatoryConstructor == null && pType.length == 2) {
                    int equals = 0;
                    for (int i = 0; i < pType.length; i++) {
                        if (HttpHeaders.class.equals(pType[i])) {
                            mandatoryParams[i] = headers;
                            equals++;
                        } else if (HttpServletResponse.class.equals(pType[i])) {
                            mandatoryParams[i] = response;
                            equals++;
                        }
                    }
                    if (equals == pType.length) {
                        mandatoryConstructor = constructor;
                    }
                }
            }
            if (context == null && mandatoryConstructor != null) {
                context = contextType.cast(mandatoryConstructor.newInstance(mandatoryParams));
            } else if (context == null) {
                throw new IllegalArgumentException(ILLEGAL_CLASS_TYPE_ERROR_MSG);
            }
            return context;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(ILLEGAL_CLASS_TYPE_ERROR_MSG);
        }
    }
}
