package com.tools.wbe.core.component.handler.abs;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.event.abs.AWebEvent;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import com.tools.wbe.data.business.wrapper.WebBusinessProperties;
import com.tools.wbe.data.common.holder.ObjectHolder;
import com.tools.wbe.data.common.spec.EWebEventStrategy;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.abs.AWebResponseContext;
import com.tools.wbe.data.event.WebBusinessActionEvent;
import com.tools.wbe.data.exception.WebBusinessException;
import org.springframework.context.ApplicationListener;
import org.springframework.data.util.Pair;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public abstract class AWebEventHandler<ACT extends AWebEvent<REQ, RES>, REQ extends AWebRequestContext, RES extends AWebResponseContext, PRO extends IWebEventProcessor>
        implements ApplicationListener<WebBusinessActionEvent> {

    private final Class<ACT> eventType;

    public AWebEventHandler(Class<ACT> eventType) {
        this.eventType = eventType;
    }

    @Override
    public void onApplicationEvent(WebBusinessActionEvent event) {
        AWebEvent webEvent = (AWebEvent) event.getSource();
        if (!(eventType.isInstance(webEvent))) {
            return;
        }
        ACT action = eventType.cast(webEvent);
        try {
            executeEvent(action);
        } catch (WebBusinessException webBusinessException) {
            webEvent.getResponseContext().setHttpStatus(webBusinessException.getHttpStatus());
            action.eventException(webBusinessException);
        }
    }

    protected abstract Map<String, List<Pair<PRO, Integer>>> getProcessorsMapping();

    protected abstract void nextProcessor(Pair<PRO, Integer> pProcessorPosition, ACT event, IWebBusinessProcess businessProcess);

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = WebBusinessException.class)
    public void executeEvent(ACT event) throws WebBusinessException {
        for (Map.Entry<String, List<Pair<PRO, Integer>>> entry : getProcessorsMapping().entrySet()) {
            if (!event.getRequestContext().getActionName().equals(entry.getKey())) {
                continue;
            }
            // prepare business process manager
            ObjectHolder<EWebEventStrategy> strategyHolder = new ObjectHolder<>(EWebEventStrategy.CONTINUE);
            IWebBusinessProcess businessProcess = new IWebBusinessProcess() {
                @Override
                public void sendStrategy(EWebEventStrategy strategy) {
                    strategyHolder.setObject(strategy);
                }

                @Override
                public WebBusinessProperties properties() {
                    return event.getBusinessProperties();
                }
            };
            // start business process
            for (Pair<PRO, Integer> actionProcessorPair : entry.getValue()) {
                nextProcessor(actionProcessorPair, event, businessProcess);
                // calculate strategy
                EWebEventStrategy actualStrategy = strategyHolder.getObject();
                if (actualStrategy == null || EWebEventStrategy.CONTINUE == actualStrategy)
                    continue;
                if (EWebEventStrategy.BREAK == actualStrategy)
                    break;
                if (EWebEventStrategy.ERROR == actualStrategy) {
                    // rollback transaction
                    throw new WebBusinessException(event.getResponseContext().getHttpStatus());
                }
            }
        }
    }
}
