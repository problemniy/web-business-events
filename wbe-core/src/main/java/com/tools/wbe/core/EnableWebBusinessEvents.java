package com.tools.wbe.core;

import com.tools.wbe.core.config.WbeManagementConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to activate Web Business Events module related configuration.
 * {@link WbeManagementConfiguration}
 *
 * @author problemniy
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(WbeManagementConfiguration.class)
public @interface EnableWebBusinessEvents {

}
