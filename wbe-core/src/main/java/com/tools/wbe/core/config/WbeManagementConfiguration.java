package com.tools.wbe.core.config;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({
        "com.tools.wbe.core",
        "com.tools.wbe.manage"
})
public class WbeManagementConfiguration {

    @Bean
    @ConditionalOnMissingBean(AWebBusinessEventsConfigAdapter.class)
    public AWebBusinessEventsConfigAdapter webBusinessEventsConfigurationAdapter() {
        return new WbeEmptyConfiguration();
    }
}
