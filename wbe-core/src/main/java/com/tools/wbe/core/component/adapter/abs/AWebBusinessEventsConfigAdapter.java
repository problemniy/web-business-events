package com.tools.wbe.core.component.adapter.abs;

import com.tools.wbe.core.config.WbeManagementConfiguration;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import org.springframework.beans.factory.InitializingBean;

/**
 * Abstract class that should be extended to configure Web Business Events module.
 * Otherwise will be used empty configuration.
 * {@link WbeManagementConfiguration}
 * It provides special builder to configure.
 * {@link #configure(WebBusinessEventsBuilder)
 *
 * @author problemniy
 */

public abstract class AWebBusinessEventsConfigAdapter implements InitializingBean {

    private final WebBusinessEventsBuilder wbeBuilder = new WebBusinessEventsBuilder();

    @Override
    public void afterPropertiesSet() {
        configure(wbeBuilder);
    }

    protected abstract void configure(WebBusinessEventsBuilder wbe);

    public WebBusinessEventsBuilder getWbeBuilder() {
        return wbeBuilder;
    }
}
