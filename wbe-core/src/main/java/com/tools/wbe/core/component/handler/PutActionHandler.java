package com.tools.wbe.core.component.handler;

import com.tools.wbe.core.component.handler.abs.AWebEventHandler;
import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.event.PutAction;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class PutActionHandler extends AWebEventHandler<PutAction, PutRequestContext, PutResponseContext, IPutActionProcessor> {

    private final Map<String, List<Pair<IPutActionProcessor, Integer>>> putActionProcessorsMapping;

    @Autowired
    public PutActionHandler(@Qualifier("wbePutActionProcessorsMapping") Map<String, List<Pair<IPutActionProcessor, Integer>>> putActionProcessorsMapping) {
        super(PutAction.class);
        this.putActionProcessorsMapping = putActionProcessorsMapping;
    }

    @Override
    protected Map<String, List<Pair<IPutActionProcessor, Integer>>> getProcessorsMapping() {
        return putActionProcessorsMapping;
    }

    @Override
    protected void nextProcessor(Pair<IPutActionProcessor, Integer> pProcessorPosition, PutAction putAction, IWebBusinessProcess businessProcess) {
        pProcessorPosition.getFirst().execute(putAction.getRequestContext(), putAction.getResponseContext(), businessProcess);
    }
}
