package com.tools.wbe.core.component.handler;

import com.tools.wbe.core.component.handler.abs.AWebEventHandler;
import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.event.GetAction;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class GetActionHandler extends AWebEventHandler<GetAction, GetRequestContext, GetResponseContext, IGetActionProcessor> {

    private final Map<String, List<Pair<IGetActionProcessor, Integer>>> getActionProcessorsMapping;

    @Autowired
    public GetActionHandler(@Qualifier("wbeGetActionProcessorsMapping") Map<String, List<Pair<IGetActionProcessor, Integer>>> getActionProcessorsMapping) {
        super(GetAction.class);
        this.getActionProcessorsMapping = getActionProcessorsMapping;
    }

    @Override
    protected Map<String, List<Pair<IGetActionProcessor, Integer>>> getProcessorsMapping() {
        return getActionProcessorsMapping;
    }

    @Override
    protected void nextProcessor(Pair<IGetActionProcessor, Integer> pProcessorPosition, GetAction getAction, IWebBusinessProcess businessProcess) {
        pProcessorPosition.getFirst().execute(getAction.getRequestContext(), getAction.getResponseContext(), businessProcess);
    }
}
