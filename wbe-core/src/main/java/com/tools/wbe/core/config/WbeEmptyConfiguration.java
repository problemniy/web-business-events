package com.tools.wbe.core.config;

import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;

import java.util.Collections;

public class WbeEmptyConfiguration extends AWebBusinessEventsConfigAdapter {

    @Override
    public void configure(WebBusinessEventsBuilder wbe) {
        wbe.setWebEventProcessorsInvolvements(Collections.emptyList());
    }
}
