package com.tools.wbe.core.component.handler;

import com.tools.wbe.core.component.handler.abs.AWebEventHandler;
import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.event.PostAction;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class PostActionHandler extends AWebEventHandler<PostAction, PostRequestContext, PostResponseContext, IPostActionProcessor> {

    private final Map<String, List<Pair<IPostActionProcessor, Integer>>> postActionProcessorsMapping;

    @Autowired
    public PostActionHandler(@Qualifier("wbePostActionProcessorsMapping") Map<String, List<Pair<IPostActionProcessor, Integer>>> postActionProcessorsMapping) {
        super(PostAction.class);
        this.postActionProcessorsMapping = postActionProcessorsMapping;
    }

    @Override
    protected Map<String, List<Pair<IPostActionProcessor, Integer>>> getProcessorsMapping() {
        return postActionProcessorsMapping;
    }

    @Override
    protected void nextProcessor(Pair<IPostActionProcessor, Integer> pProcessorPosition, PostAction postAction, IWebBusinessProcess businessProcess) {
        pProcessorPosition.getFirst().execute(postAction.getRequestContext(), postAction.getResponseContext(), businessProcess);
    }
}
