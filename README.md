## Web Business Events

![Project Status: Active](https://img.shields.io/badge/Project_Status-Active-darkgreen.svg)
![Spring Boot](https://img.shields.io/badge/-Spring%20Boot-6DB33F?logo=spring-boot&logoColor=white)
![FreeMarker](https://img.shields.io/badge/-FreeMarker-0052CC?logo=freemarker&logoColor=white)
![Java](https://img.shields.io/badge/Java-%23ED8B00.svg?logo=openjdk&logoColor=white)
![Gradle](https://img.shields.io/badge/-Gradle-02303A?logo=gradle&logoColor=white)

A Spring Boot module that simplifies handling incoming HTTP requests, making it more declarative and straightforward

- Provides a convenient interface for handling incoming HTTP requests and generating responses
- Utilizes a declarative approach to process the corresponding HTTP requests
- Uses actions that sequentially invoke processors (applying the *Chain of Responsibility* pattern), which handle
  incoming
  HTTP requests and prepare data for the response in a sequential manner
- Maps processors to the custom actions through the configuration of corresponding beans in an XML file
- Provides a user-friendly view in the UI where all custom actions with mappings of processors to corresponding HTTP
  requests can be viewed, displayed in the order defined by preconfigured settings

## Hello Worlds!

To compile the module, you need to:

- Use Java 11 version
- Use Gradle 6.7 version

### Quick Start

- Add the module in your project

```groovy
dependencies {
    implementation group: 'com.gitlab.problemniy', name: 'web-business-events', version: '-SNAPSHOT'
}
```

The module is distributed via [JitPack](https://jitpack.io), so it's also necessary to include the corresponding
repository in the Gradle configuration

```groovy
repositories {
    mavenCentral()
    maven { url "https://jitpack.io" }
}
```

- Add an [@EnableWebBusinessEvents](wbe-core/src/main/java/com/tools/wbe/core/EnableWebBusinessEvents.java) annotation
  to activate the module in your project
- Create an XML configuration in your resource folder (e.g. *wbe-processors.config.xml*)
- Create bean classes implementing the interface for the corresponding HTTP request type

*An example of configuration*

```xml

<context:component-scan
        base-package="com.webapp.dat.core.config, com.webapp.dat.core.component.wbe.processor"/>

<beans xmlns="http://www.springframework.org/schema/beans">
    <bean class="com.tools.wbe.data.business.WebEventProcessorInvolvement">
      
        <property name="processor" ref="updateUserProcessor"/>
    
        <property name="eventTypes">
            <util:list>
                <value type="com.tools.wbe.data.common.spec.EWbeRequestMethod">POST</value>
                <value type="com.tools.wbe.data.common.spec.EWbeRequestMethod">PUT</value>
            </util:list>
        </property>
    
        <property name="actions">
            <util:list>
                <util:constant
                        static-field="UPDATE_USER"/>
            </util:list>
        </property>
    
        <property name="order" value="5"/>
    
    </bean>
</beans>
```

### Explanation

- *'processor'* property defines a reference to a bean implementing the
  interface [IWebEventProcessor](wbe-data/src/main/java/com/tools/wbe/data/business/processor/IWebEventProcessor.java),
  corresponding to the type of HTTP request being handled
- *'eventTypes'* property defines the list of HTTP requests that this processor will work with
- *'actions'* property defines a list of custom event names to which this processor will be mapped
- *'order'* property determines the position of the processor in the chain of other processors that handle the same
  combination of events and HTTP request types

*An example of a simple processor class*

```java

@Component("updateUserProcessor")
public class UpdateUserProcessor implements IPostActionProcessor, IPutActionProcessor {

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
        User updatedUser;
        // some logic
        postResponseContext.setBody(updatedUser);
    }

    @Override
    public void execute(PutRequestContext putRequestContext, PutResponseContext putResponseContext, IWebBusinessProcess iWebBusinessProcess) {
        User updatedUser;
        // some logic
        putResponseContext.setBody(updatedUser);
    }
}
```

Note that each HTTP request type corresponds to its own interface with the corresponding
[IWebBodyContext](wbe-data/src/main/java/com/tools/wbe/data/context/type/IWebBodyContext.java)

To execute the action, which performs a chain of operations on the incoming HTTP request data and generates a response,
you need to use a builder of the corresponding type, for example

```java

@Service
public class BusinessManagementService {

    private final PostActionExecutor postActionExecutor;
    private final WebBusinessEventsService webBusinessEventsService;

    public BusinessManagementService(PostActionExecutor postActionExecutor) {
        this.postActionExecutor = postActionExecutor;
    }

    public ResponseEntity<User> updateUser(User user, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return postActionExecutor.prepare(
                        () -> webBusinessEventsService.prepareRequestContext(headers, request, user, PostRequestContext.class),
                        () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, PostResponseContext.class))
                .addAction("UPDATE_USER")
                .go()
                .asResponseEntity(User.class);
    }
}
```

### Explanation

- *'webBusinessEventsService.prepareRequestContext'* forms the request context sent to the event processor chain
- *'webBusinessEventsService.prepareResponseContext'* forms the response context where processors gradually build the
  response data (headers, body, status, etc.)
- *'addAction'* specifies event names which will be triggered for the corresponding HTTP type
- *'go'* initiates the execution of the processor chain and response data generation
- *'asResponseEntity'* specifies the format of the returned response, indicating to immediately return a
  ResponseEntity<User> with the formed body, headers and status code

## UI View

The module provides a user-friendly UI view where you can see the generated mapping of processors in the specified order
by HTTP request types and event names. To open the page, use URI

    [HOST]/wbe/mapping

Note: If necessary, you can add restrictions to this URI by using Spring Security to limit access for regular users

## Building from Source

[Gradle](https://www.gradle.org) is used to build, test, and publish. JDK 1.11 or higher is required.
<br>To build the module, run:

    ./gradlew clean build

## Notes

The processor can change the execution strategy of a chain of calls to other processors by using the
received [IWebBusinessProcess](wbe-data/src/main/java/com/tools/wbe/data/business/delegate/IWebBusinessProcess.java)
object as an argument and invoking its *#sendStrategy* method
<br><br>There are a total of 3 predefined strategy options in
the [EWebEventStrategy](wbe-data/src/main/java/com/tools/wbe/data/common/spec/EWebEventStrategy.java) class

- *CONTINUE* - The chain of processors continues execution (default behavior)
- *BREAK* - The chain is interrupted after the current processor is executed, returning the formed result
- *ERROR* - The chain is interrupted and an
  error [WebBusinessException](wbe-data/src/main/java/com/tools/wbe/data/exception/WebBusinessException.java) is thrown

Note: The *WebBusinessException* utilizes the current status of the response context, formed by the processors, to
substitute it into the returned HTTP response (if necessary, the status can be explicitly specified when throwing the
exception)

The processor can also pass transient attributes between other processors in the chain using *#properties* method from
the *IWebBusinessProcess* object to obtain
the [WebBusinessProperties](wbe-data/src/main/java/com/tools/wbe/data/business/wrapper/WebBusinessProperties.java)
class, which stores transient data

## Author

@problemniy
